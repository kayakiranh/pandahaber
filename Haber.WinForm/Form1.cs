﻿using Haber.Business;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Haber.WinForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {

            #region EN GÜNCEL İE VERSİYONUNU GETİR

            int BrowserVer, RegVal;

            using (WebBrowser Wb = new WebBrowser())
                BrowserVer = Wb.Version.Major;

            if (BrowserVer >= 11)
                RegVal = 11001;
            else if (BrowserVer == 10)
                RegVal = 10001;
            else if (BrowserVer == 9)
                RegVal = 9999;
            else if (BrowserVer == 8)
                RegVal = 8888;
            else
                RegVal = 7000;

            using (RegistryKey Key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION", RegistryKeyPermissionCheck.ReadWriteSubTree))
                if (Key.GetValue(System.Diagnostics.Process.GetCurrentProcess().ProcessName + ".exe") == null)
                    Key.SetValue(System.Diagnostics.Process.GetCurrentProcess().ProcessName + ".exe", RegVal, RegistryValueKind.DWord);

            #endregion

            InitializeComponent();

            toolStripStatusLabel1.Text = "HK HABER BOT : 1.1";
        }

        #region ASENKRON METOD - BUTTON14 - ASENKRON HELPER

        private void button14_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "SENKTRON İLK TARAMA BAŞLADI";

            SyncStart();

            toolStripStatusLabel1.Text = "SENKTRON İLK TARAMA BİTTİ";

            toolStripStatusLabel1.Text = "HER 30 DAKİKADA Bİ RUTİN TARAMA MODU AÇIK";

            timer1.Interval = 20 * 60 * 1000; //20 DAKİKADA BİR ÇALIŞ
            timer1.Enabled = true;
            timer1.Start();
            timer1.Tick += new EventHandler(timer1_Tick);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "RUTİN TARAMA BAŞLADI";
            SyncStart();
            toolStripStatusLabel1.Text = "RUTİN TARAMA BİTTİ";
        }

        private void SyncStart()
        {
            BotService BS = new BotService();
            Task task1 = Task.Factory.StartNew(() => BS.SabahBot());
            Task task2 = Task.Factory.StartNew(() => BS.MilliyetBot());
            //Task task3 = Task.Factory.StartNew(() => BS.HurriyetBot());
            //Task task4 = Task.Factory.StartNew(() => BS.SozcuBot());
            Task task5 = Task.Factory.StartNew(() => BS.YeniAkitBot());
            Task task6 = Task.Factory.StartNew(() => BS.FanatikBot());
            Task task7 = Task.Factory.StartNew(() => BS.FotomacBot());
            Task task8 = Task.Factory.StartNew(() => BS.PostaBot());

            //Task.WaitAll(task1, task2, task3, task4, task5, task6, task7, task8);
            Task.WaitAll(task1, task2, task5, task6, task7, task8);
        }

        #endregion

        //HÜRRİYET, DATA GELMİYO, WAİT METODUNU KULLAN
        private void button13_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "HÜRRİYET BAŞLADI";

            BotService BS = new BotService();
            var sl = BS.HurriyetBotListesi();

            for (int i = 0; i < sl.Count; i++)
            {
                toolStripStatusLabel1.Text = "HÜRRİYET DEVAM EDİYOR";

                WebBrowser wb = new WebBrowser();
                wb.Name = sl[i].ToString();
                wb.Visible = false;
                wb.Width = 0;
                wb.Height = 0;
                wb.ScriptErrorsSuppressed = true;
                this.Controls.Add(wb);

                string[] _sl = sl[i].Split(',');
                var uri = _sl[0];
                wb.ScriptErrorsSuppressed = true;
                wb.Navigate(uri);

                waitTillLoad(wb);

                BS.HurriyetHaberTaraForm(wb.Name.Split(',')[0], wb.Name.Split(',')[1], wb.Name.Split(',')[2], wb.DocumentText.ToString());

                wb.Dispose();
            }

            toolStripStatusLabel1.Text = "HÜRRİYET BİTTİ";
        }

        //SÖZCÜ, DATA GELMİYO, WAİT METODUNU KULLAN
        private void button1_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "SÖZCÜ BAŞLADI";

            BotService BS = new BotService();
            //var sl = BS.SozcuHaberListesi();

            List<string> haberList = new List<string>();

            //sözcü kategorileri tara()
            List<Kategori> sozcuKategori = BS.SozcuKategoriListesi();
            for (int i = 1; i < sozcuKategori.Count; i++)
            {
                toolStripStatusLabel1.Text = "SÖZCÜ DEVAM EDİYOR";

                WebBrowser wb = new WebBrowser();
                wb.Name = sozcuKategori[i].ToString();
                wb.Visible = false;
                wb.Width = 0;
                wb.Height = 0;
                wb.ScriptErrorsSuppressed = true;
                this.Controls.Add(wb);

                wb.ScriptErrorsSuppressed = true;
                wb.Navigate(sozcuKategori[i].KategoriURL);

                waitTillLoad(wb);

                List<string> haberByCat = BS.LinkListesiForAngular("sozcu", sozcuKategori[i].KategoriURL, sozcuKategori[i], wb.DocumentText.ToString(), "//div[@class='category-content']");

                haberList.AddRange(haberByCat);
                wb.Dispose();
            }

            //sözcü haber tara


            //taranan kategorileriden haberleri tara
            for (int i = 0; i < haberList.Count; i++)
            {
                toolStripStatusLabel1.Text = "SÖZCÜ DEVAM EDİYOR";

                WebBrowser wb = new WebBrowser();
                wb.Name = haberList[i].ToString();
                wb.Visible = false;
                wb.Width = 0;
                wb.Height = 0;
                wb.ScriptErrorsSuppressed = true;
                this.Controls.Add(wb);

                string[] _sl = haberList[i].Split(',');
                var uri = _sl[0];
                wb.ScriptErrorsSuppressed = true;
                wb.Navigate(uri);

                waitTillLoad(wb);

                BS.SozcuHaberTaraForm(wb.Name.Split(',')[0], wb.Name.Split(',')[1], wb.Name.Split(',')[2], wb.DocumentText.ToString());

                wb.Dispose();
            }

            toolStripStatusLabel1.Text = "SÖZCÜ BİTTİ";
        }

        //SONRADAN YÜKLENEN SAYFALARDA DATALARIN GELMEME SORUNUNU ÇÖZEN KOD BLOĞU
        //10 SANİYE + 10'NCU SANİYEDEN İTİBAREN COMPLETE OLMADIĞI HER DÖNGÜDE SÜREYİ UZATIYO
        private void waitTillLoad(WebBrowser webBrControl)
        {
            WebBrowserReadyState loadStatus;
            int waittime = 100000;
            int counter = 0;
            while (true)
            {
                loadStatus = webBrControl.ReadyState;
                Application.DoEvents();
                if ((counter > waittime) || (loadStatus == WebBrowserReadyState.Uninitialized) || (loadStatus == WebBrowserReadyState.Loading) || (loadStatus == WebBrowserReadyState.Interactive))
                {
                    break;
                }
                counter++;
            }

            counter = 0;
            while (true)
            {
                loadStatus = webBrControl.ReadyState;
                Application.DoEvents();
                if (loadStatus == WebBrowserReadyState.Complete && webBrControl.IsBusy != true)
                {
                    break;
                }
                counter++;
            }
        }

        //SABAH
        private void button12_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "SABAH BAŞLADI";
            BotService BS = new BotService();
            toolStripStatusLabel1.Text = "SABAH DEVAM EDİYOR";
            BS.SabahBot();
            toolStripStatusLabel1.Text = "SABAH BİTTİ";
        }

        //MİLLİYET
        private void button11_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "MİLLİYET BAŞLADI";
            BotService BS = new BotService();
            toolStripStatusLabel1.Text = "MİLLİYET DEVAM EDİYOR";
            BS.MilliyetBot();
            toolStripStatusLabel1.Text = "MİLLİYET BİTTİ";
        }

        //YENİ AKİT
        private void button10_Click(object sender, EventArgs e)
        {

        }

        //FANATİK
        private void button9_Click(object sender, EventArgs e)
        {

        }

        //FOTOMAÇ
        private void button8_Click(object sender, EventArgs e)
        {

        }

        //POSTA
        private void button7_Click(object sender, EventArgs e)
        {

        }

        //ESKI HABERLERI UCUR
        private void button6_Click(object sender, EventArgs e)
        {
            HaberService HS = new HaberService();
            HS.clear();
        }

        //ÇIKIŞ YAP
        private void button5_Click(object sender, EventArgs e)
        {
            Environment.Exit(1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "HER 30 DAKİKADA Bİ RUTİN TARAMA MODU KAPANDI";
            timer1.Stop();
        }
    }
}