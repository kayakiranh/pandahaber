﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Haber.Business
{
    public class BotService
    {
        #region SABAH

        private static List<Kategori> finalSabahCategoryList = new List<Kategori>();
        private static int sabahHaberSay = 0;

        public void SabahHaberTara(string url, string kategori)
        {
            try
            {
                var getHtmlWeb = new HtmlWeb();
                var document = getHtmlWeb.Load(url);
                var newAreas = document.DocumentNode.SelectNodes("//div[@class='row topDetail']");
                if (newAreas != null)
                {
                    foreach (var newArea in newAreas)
                    {
                        HaberService HS = new HaberService();
                        try
                        {
                            string _haberKaynak = "sabah";
                            string _haberKategori = url.Split('/')[3];
                            string _haberLink = url.ToString();
                            string _haberBaslik = document.DocumentNode.SelectSingleNode("//h2[@class='spot']").InnerText;
                            string _haberBaslikSeo = DoFriendly(document.DocumentNode.SelectSingleNode("//meta[@itemprop='alternativeHeadline']").Attributes["content"].Value);
                            string _haberGorsel = document.DocumentNode.SelectSingleNode("//meta[@itemprop='image']").Attributes["content"].Value;
                            string _haberIcerik = Regex.Replace(Regex.Replace(document.DocumentNode.SelectSingleNode("//div[@class='newsBox']").InnerHtml, "<.*?>", String.Empty), @"\s+", " ").Trim().Replace("GÜNÜN EN ÖNEMLİ MANŞETLERİ İÇİN TIKLAYIN BUGÜN NELER OLDU", "");
                            string _haberEtiket = document.DocumentNode.SelectSingleNode("//meta[@name='news_keywords']").Attributes["content"].Value;
                            string _haberOzet = "";
                            int _haberPuan = 0;
                            string _haberNot = (GoogleTrendIleKarsilastir(_haberIcerik) ? "Trend" : "");
                            DateTime _haberEklenme = Convert.ToDateTime(document.DocumentNode.SelectSingleNode("//meta[@itemprop='datePublished']").Attributes["content"].Value.Replace("T", " ").Replace("Z", ""));
                            string _haberMetaBaslik = document.DocumentNode.SelectSingleNode("//meta[@itemprop='Description']").Attributes["content"].Value;
                            string _haberMetaKisaBaslik = document.DocumentNode.SelectSingleNode("//meta[@itemprop='alternativeHeadline']").Attributes["content"].Value;
                            string _haberMetaGorsel = document.DocumentNode.SelectSingleNode("//meta[@name='thumbnail']").Attributes["content"].Value;
                            int _haberOkunmaSuresi = OkunmaSuresiHesapla(_haberIcerik);
                            string _haberKaynakAdi = "Sabah";
                            string _haberKategoriAdi = kategori.Replace("-", "/");

                            HS.insert(_haberKaynak, _haberKategori, _haberLink, _haberBaslik, _haberBaslikSeo, _haberGorsel, _haberIcerik, _haberEtiket, _haberOzet, _haberPuan, _haberNot, _haberMetaBaslik, _haberMetaKisaBaslik, _haberMetaGorsel, _haberEklenme, _haberOkunmaSuresi, _haberKaynakAdi, _haberKategoriAdi);

                            sabahHaberSay++;
                        }
                        catch { }
                    }
                }
                else { }
            }
            catch { }
        }

        public int SabahBot()
        {
            KategoriTara("http://sabah.com.tr", "//div[@class='menuSideBar']", finalSabahCategoryList);
            LinkTara("sabah", "http://sabah.com.tr", finalSabahCategoryList, "//div[@class='container']");
            return sabahHaberSay;
        }

        #endregion SABAH

        #region MILLIYET

        private static List<Kategori> finalMilliyetCategoryList = new List<Kategori>();
        private static int milliyetHaberSay = 0;

        public void MilliyetHaberTara(string url, string kategoriUrl, string kategori)
        {
            try
            {
                var getHtmlWeb = new HtmlWeb();
                var document = getHtmlWeb.Load(url);
                var newAreas = document.DocumentNode.SelectNodes("//div[@class='colA']");
                if (newAreas != null)
                {
                    foreach (var newArea in newAreas)
                    {
                        HaberService HS = new HaberService();
                        try
                        {
                            string _haberKaynak = "milliyet";
                            string _haberKategori = kategoriUrl;
                            string _haberLink = url.ToString();
                            string _haberBaslik = document.DocumentNode.SelectSingleNode("//h1[@itemprop='headline']").InnerText;
                            string _haberBaslikSeo = DoFriendly(document.DocumentNode.SelectSingleNode("//h1[@itemprop='headline']").InnerText);

                            string _haberGorsel = "";
                            try { _haberGorsel = (document.DocumentNode.SelectSingleNode("//img[@class='image']").Attributes["src"].Value); }
                            catch { _haberGorsel = (document.DocumentNode.SelectSingleNode("//img[@itemprop='url']").Attributes["src"].Value); }

                            string _haberIcerik = Regex.Replace(Regex.Replace(document.DocumentNode.SelectSingleNode("//div[@itemprop='articleBody']").InnerHtml, "<.*?>", String.Empty), @"\s+", " ").Trim().Replace("GÜNÜN EN ÖNEMLİ MANŞETLERİ İÇİN TIKLAYIN BUGÜN NELER OLDU", "");
                            string _haberEtiket = document.DocumentNode.SelectSingleNode("//div[@class='etiketler']").InnerText;
                            string _haberOzet = "";
                            int _haberPuan = 0;
                            string _haberNot = (GoogleTrendIleKarsilastir(_haberIcerik) ? "Trend" : "");
                            DateTime _haberEklenme = DateTime.Now;

                            try { _haberEklenme = Convert.ToDateTime((document.DocumentNode.SelectSingleNode("//div[@class='date']").InnerText).Substring(document.DocumentNode.SelectSingleNode("//div[@class='date']").InnerText.Length - 16)); }
                            catch { _haberEklenme = Convert.ToDateTime((document.DocumentNode.SelectSingleNode("//div[@class='dt']").InnerText).Replace("|","").Replace(" ", "").Replace("-", " ").Substring((document.DocumentNode.SelectSingleNode("//div[@class='dt']").InnerText).Replace("|", "").Replace(" ", "").Replace("-", " ").Length - 16)); }

                            string _haberMetaBaslik = document.DocumentNode.SelectSingleNode("//meta[@property='og:title']").Attributes["content"].Value.Replace("&#39;", "'");
                            string _haberMetaKisaBaslik = document.DocumentNode.SelectSingleNode("//meta[@property='og:description']").Attributes["content"].Value.Replace("&#39;", "'");
                            string _haberMetaGorsel = document.DocumentNode.SelectSingleNode("//meta[@property='og:image']").Attributes["content"].Value.Replace("&#39;", "'");
                            int _haberOkunmaSuresi = OkunmaSuresiHesapla(_haberIcerik);
                            string _haberKaynakAdi = "Milliyet";
                            string _haberKategoriAdi = kategori.Replace("-", "/");

                            HS.insert(_haberKaynak, _haberKategori, _haberLink, _haberBaslik, _haberBaslikSeo, _haberGorsel, _haberIcerik, _haberEtiket, _haberOzet, _haberPuan, _haberNot, _haberMetaBaslik, _haberMetaKisaBaslik, _haberMetaGorsel, _haberEklenme, _haberOkunmaSuresi, _haberKaynakAdi, _haberKategoriAdi);
                            milliyetHaberSay++;
                        }
                        catch { }
                    }
                }
                else { }
            }
            catch { }
        }

        public int MilliyetBot()
        {
            KategoriTara("http://milliyet.com.tr", "//div[@class='links']", finalMilliyetCategoryList);
            LinkTara("milliyet", "http://milliyet.com.tr", finalMilliyetCategoryList, "//div[@class='colA']");
            return milliyetHaberSay;
        }

        #endregion MILLIYET

        #region HURRIYET

        private static List<Kategori> finalHurriyetCategoryList = new List<Kategori>();
        private static int hurriyetHaberSay = 0;

        #region HÜRRİYET DÜZ BOT, SADECE META OKUYABİLİYO

        //public void HurriyetHaberTara(string url, string kategoriUrl, string kategori)
        //{
        //    try
        //    {
        //        var getHtmlWeb = new HtmlWeb();
        //        var document = getHtmlWeb.Load(url);
        //        var newAreas = document.DocumentNode.SelectNodes("//div[@class='left-block-620']");
        //        if (newAreas != null)
        //        {
        //            foreach (var newArea in newAreas)
        //            {
        //                HaberService HS = new HaberService();
        //                try
        //                {
        //                    string _haberKaynak = "hurriyet";
        //                    string _haberKategori = kategoriUrl;
        //                    string _haberLink = url.ToString();
        //                    string _haberBaslik = document.DocumentNode.SelectSingleNode("//meta[@property='og:title']").Attributes["content"].Value.Replace("&#39;", "'");
        //                    string _haberBaslikSeo = DoFriendly(document.DocumentNode.SelectSingleNode("//meta[@property='og:title']").Attributes["content"].Value.Replace("&#39;", "'"));
        //                    string _haberGorsel = (document.DocumentNode.SelectSingleNode("//img[@itemprop='url']").Attributes["src"].Value);
        //                    string _haberIcerik = document.DocumentNode.SelectSingleNode("//meta[@property='og:description']").Attributes["content"].Value.Replace("&#39;", "'");
        //                    string _haberEtiket = document.DocumentNode.SelectSingleNode("//div[@class='tagsContainer']").InnerText;

        //                    string _haberOzet = "";
        //                    int _haberPuan = 0;
        //                    string _haberNot = (GoogleTrendIleKarsilastir(_haberIcerik) ? "Trend" : "");
        //                    DateTime _haberEklenme = DateTime.Now;

        //                    string _haberMetaBaslik = document.DocumentNode.SelectSingleNode("//meta[@property='og:title']").Attributes["content"].Value.Replace("&#39;", "'");
        //                    string _haberMetaKisaBaslik = document.DocumentNode.SelectSingleNode("//meta[@property='og:description']").Attributes["content"].Value.Replace("&#39;", "'");
        //                    string _haberMetaGorsel = document.DocumentNode.SelectSingleNode("//meta[@itemprop='image']").Attributes["content"].Value.Replace("&#39;", "'");
        //                    int _haberOkunmaSuresi = OkunmaSuresiHesapla(_haberIcerik);
        //                    string _haberKaynakAdi = "Hürriyet";
        //                    string _haberKategoriAdi = kategori.Replace("-", "/");

        //                    HS.insert(_haberKaynak, _haberKategori, _haberLink, _haberBaslik, _haberBaslikSeo, _haberGorsel, _haberIcerik, _haberEtiket, _haberOzet, _haberPuan, _haberNot, _haberMetaBaslik, _haberMetaKisaBaslik, _haberMetaGorsel, _haberEklenme, _haberOkunmaSuresi, _haberKaynakAdi, _haberKategoriAdi);
        //                    hurriyetHaberSay++;
        //                }
        //                catch { }
        //            }
        //        }
        //        else { }
        //    }
        //    catch { }
        //}

        //public int HurriyetBot()
        //{
        //    KategoriTara("http://hurriyet.com.tr", "//ul[@class='main-menu-list cf']", finalHurriyetCategoryList);
        //    LinkTara("hurriyet", "http://hurriyet.com.tr", finalHurriyetCategoryList, "//div[@class='page-wrapper']");
        //    return hurriyetHaberSay;
        //}

        #endregion

        #region HÜRRİYET FORMDAN GELEN DATALARLA ÇALIŞAN BOT

        //TODO : http://hurriyet.com.tr/enerji/ bu link incelenicek
        //TODO : http://hurriyet.com.tr/emlak/ bu link incelenicek

        public void HurriyetHaberTaraForm(string url, string kategoriUrl, string kategori, string doc)
        {
            try
            {
                HtmlDocument document = new HtmlDocument();
                document.LoadHtml(doc);
                var newAreas = document.DocumentNode.SelectNodes("//div[@class='left-block-620']");
                if (newAreas != null)
                {
                    foreach (var newArea in newAreas)
                    {
                        HaberService HS = new HaberService();
                        try
                        {
                            string _haberKaynak = "hurriyet";
                            string _haberKategori = kategoriUrl;
                            string _haberLink = url.ToString();
                            string _haberBaslik = document.DocumentNode.SelectSingleNode("//meta[@property='og:title']").Attributes["content"].Value.Replace("&#39;", "'");
                            string _haberBaslikSeo = DoFriendly(document.DocumentNode.SelectSingleNode("//meta[@property='og:title']").Attributes["content"].Value.Replace("&#39;", "'"));
                            string _haberGorsel = (document.DocumentNode.SelectSingleNode("//meta[@itemprop='image']").Attributes["content"].Value);

                            string _haberIcerik = document.DocumentNode.SelectSingleNode("//div[@class='news-box']").InnerText;
                            string _haberEtiket = document.DocumentNode.SelectSingleNode("//div[@class='tagsContainer']").InnerText;

                            string _haberOzet = "";
                            int _haberPuan = 0;
                            string _haberNot = (GoogleTrendIleKarsilastir(_haberIcerik) ? "Trend" : "");
                            DateTime _haberEklenme = DateTime.Now;

                            string _haberMetaBaslik = document.DocumentNode.SelectSingleNode("//meta[@property='og:title']").Attributes["content"].Value.Replace("&#39;", "'");
                            string _haberMetaKisaBaslik = document.DocumentNode.SelectSingleNode("//meta[@property='og:description']").Attributes["content"].Value.Replace("&#39;", "'");
                            string _haberMetaGorsel = document.DocumentNode.SelectSingleNode("//meta[@itemprop='image']").Attributes["content"].Value.Replace("&#39;", "'");
                            int _haberOkunmaSuresi = OkunmaSuresiHesapla(_haberIcerik);
                            string _haberKaynakAdi = "Hürriyet";
                            string _haberKategoriAdi = kategori.Replace("-", "/");

                            HS.insert(_haberKaynak, _haberKategori, _haberLink, _haberBaslik, _haberBaslikSeo, _haberGorsel, _haberIcerik, _haberEtiket, _haberOzet, _haberPuan, _haberNot, _haberMetaBaslik, _haberMetaKisaBaslik, _haberMetaGorsel, _haberEklenme, _haberOkunmaSuresi, _haberKaynakAdi, _haberKategoriAdi);
                            hurriyetHaberSay++;
                        }
                        catch { }
                    }
                }
                else { }
            }
            catch { }
        }

        public List<string> HurriyetBotListesi()
        {
            List<string> sl = new List<string>();
            KategoriTara("http://hurriyet.com.tr", "//ul[@class='main-menu-list cf']", finalHurriyetCategoryList);
            sl = LinkListesi("hurriyet", "http://hurriyet.com.tr", finalHurriyetCategoryList, "//div[@class='page-wrapper']");
            return sl;
        }

        #endregion

        #endregion HURRIYET

        #region SOZCU

        private static List<Kategori> finalSozcuCategoryList = new List<Kategori>();
        private static int sozcuHaberSay = 0;

        #region SÖZCÜ DÜZ BOT, SADECE META OKUYABİLİYO

        //public void SozcuHaberTara(string url, string kategoriUrl, string kategori)
        //{
        //    try
        //    {
        //        var getHtmlWeb = new HtmlWeb();
        //        var document = getHtmlWeb.Load(url);
        //        var newAreas = document.DocumentNode.SelectNodes("//div[@class='detail-content-inner']");
        //        if (newAreas != null)
        //        {
        //            foreach (var newArea in newAreas)
        //            {
        //                HaberService HS = new HaberService();
        //                try
        //                {
        //                    string _haberKaynak = "Sozcu";
        //                    string _haberKategori = kategoriUrl;
        //                    string _haberLink = url.ToString();
        //                    string _haberBaslik = document.DocumentNode.SelectSingleNode("//h1[@itemprop='headline']").InnerText;
        //                    string _haberBaslikSeo = DoFriendly(document.DocumentNode.SelectSingleNode("//h1[@itemprop='headline']").InnerText);

        //                    string _haberGorsel = "";
        //                    try { _haberGorsel = (document.DocumentNode.SelectSingleNode("//img[@class='image']").Attributes["src"].Value); }
        //                    catch { _haberGorsel = (document.DocumentNode.SelectSingleNode("//img[@itemprop='url']").Attributes["src"].Value); }

        //                    string _haberIcerik = Regex.Replace(Regex.Replace(document.DocumentNode.SelectSingleNode("//div[@itemprop='articleBody']").InnerHtml, "<.*?>", String.Empty), @"\s+", " ").Trim().Replace("GÜNÜN EN ÖNEMLİ MANŞETLERİ İÇİN TIKLAYIN BUGÜN NELER OLDU", "");
        //                    string _haberEtiket = document.DocumentNode.SelectSingleNode("//div[@class='etiketler']").InnerText;
        //                    string _haberOzet = "";
        //                    int _haberPuan = 0;
        //                    string _haberNot = (GoogleTrendIleKarsilastir(_haberIcerik) ? "Trend" : "");
        //                    DateTime _haberEklenme = DateTime.Now;

        //                    try { _haberEklenme = Convert.ToDateTime((document.DocumentNode.SelectSingleNode("//div[@class='date']").InnerText).Substring(document.DocumentNode.SelectSingleNode("//div[@class='date']").InnerText.Length - 16)); }
        //                    catch { _haberEklenme = Convert.ToDateTime((document.DocumentNode.SelectSingleNode("//div[@class='dt']").InnerText).Replace("|", "").Replace(" ", "").Replace("-", " ").Substring((document.DocumentNode.SelectSingleNode("//div[@class='dt']").InnerText).Replace("|", "").Replace(" ", "").Replace("-", " ").Length - 16)); }

        //                    string _haberMetaBaslik = document.DocumentNode.SelectSingleNode("//meta[@property='og:title']").Attributes["content"].Value.Replace("&#39;", "'");
        //                    string _haberMetaKisaBaslik = document.DocumentNode.SelectSingleNode("//meta[@property='og:description']").Attributes["content"].Value.Replace("&#39;", "'");
        //                    string _haberMetaGorsel = document.DocumentNode.SelectSingleNode("//meta[@property='og:image']").Attributes["content"].Value.Replace("&#39;", "'");
        //                    int _haberOkunmaSuresi = OkunmaSuresiHesapla(_haberIcerik);
        //                    string _haberKaynakAdi = "Sozcu";
        //                    string _haberKategoriAdi = kategori.Replace("-", "/");

        //                    HS.insert(_haberKaynak, _haberKategori, _haberLink, _haberBaslik, _haberBaslikSeo, _haberGorsel, _haberIcerik, _haberEtiket, _haberOzet, _haberPuan, _haberNot, _haberMetaBaslik, _haberMetaKisaBaslik, _haberMetaGorsel, _haberEklenme, _haberOkunmaSuresi, _haberKaynakAdi, _haberKategoriAdi);
        //                    sozcuHaberSay++;
        //                }
        //                catch { }
        //            }
        //        }
        //        else
        //        { }
        //    }
        //    catch { }
        //}

        //public int SozcuBot()
        //{
        //    KategoriTara("http://sozcu.com.tr", "//div[@class='main-menu']", finalSozcuCategoryList);
        //    LinkTara("sozcu", "http://sozcu.com.tr", finalSozcuCategoryList, "//div[@class='category-right-inner']");
        //    return sozcuHaberSay;
        //}

        #endregion

        #region SÖZCÜ FORMDAN GELEN DATALARLA ÇALIŞAN BOT

        public void SozcuHaberTaraForm(string url, string kategoriUrl, string kategori, string doc)
        {
            try
            {
                HtmlDocument document = new HtmlDocument();
                document.LoadHtml(doc);
                var newAreas = document.DocumentNode.SelectNodes("//div[@class='detail-content-inner']");
                if (newAreas != null)
                {
                    foreach (var newArea in newAreas)
                    {
                        HaberService HS = new HaberService();
                        try
                        {
                            string _haberKaynak = "sozcu";
                            string _haberKategori = kategoriUrl;
                            string _haberLink = url.ToString();
                            string _haberBaslik = document.DocumentNode.SelectSingleNode("//class[@class='news-detail-title']").InnerText;
                            string _haberBaslikSeo = DoFriendly(document.DocumentNode.SelectSingleNode("//class[@class='news-detail-title']").InnerText);

                            string _haberGorsel = (document.DocumentNode.SelectSingleNode("//div[@class='detail-news-img']//img").Attributes["src"].Value);
                            string _haberIcerik = Regex.Replace(Regex.Replace(document.DocumentNode.SelectSingleNode("//div[@class='news-body']").InnerHtml, "<.*?>", String.Empty), @"\s+", " ").Trim().Replace("GÜNÜN EN ÖNEMLİ MANŞETLERİ İÇİN TIKLAYIN BUGÜN NELER OLDU", "");
                            string _haberEtiket = document.DocumentNode.SelectSingleNode("//meta[@name='keywords']").Attributes["content"].Value;
                            string _haberOzet = "";
                            int _haberPuan = 0;
                            string _haberNot = (GoogleTrendIleKarsilastir(_haberIcerik) ? "Trend" : "");

                            DateTime _haberEklenme = Convert.ToDateTime((document.DocumentNode.SelectSingleNode("//meta[@http-equiv='last-modified']").Attributes["content"].Value.Replace("T", " ").Substring((document.DocumentNode.SelectSingleNode("//meta[@http-equiv='last-modified']").Attributes["content"].Value.Length - 5))));

                            string _haberMetaBaslik = document.DocumentNode.SelectSingleNode("//meta[@property='og:title']").Attributes["content"].Value.Replace("&#39;", "'");
                            string _haberMetaKisaBaslik = document.DocumentNode.SelectSingleNode("//meta[@name='og:description']").Attributes["content"].Value.Replace("&#39;", "'");
                            string _haberMetaGorsel = document.DocumentNode.SelectSingleNode("//meta[@property='og:image']").Attributes["content"].Value.Replace("&#39;", "'");
                            int _haberOkunmaSuresi = OkunmaSuresiHesapla(_haberIcerik);
                            string _haberKaynakAdi = "Sözcü";
                            string _haberKategoriAdi = kategori.Replace("-", "/");

                            HS.insert(_haberKaynak, _haberKategori, _haberLink, _haberBaslik, _haberBaslikSeo, _haberGorsel, _haberIcerik, _haberEtiket, _haberOzet, _haberPuan, _haberNot, _haberMetaBaslik, _haberMetaKisaBaslik, _haberMetaGorsel, _haberEklenme, _haberOkunmaSuresi, _haberKaynakAdi, _haberKategoriAdi);
                            sozcuHaberSay++;
                        }
                        catch { }
                    }
                }
                else { }
            }
            catch { }
        }

        public List<Kategori> SozcuKategoriListesi()
        {
            List<string> sl = new List<string>();
            KategoriTara("http://sozcu.com.tr", "//div[@class='main-menu']", finalSozcuCategoryList);
            //sl = LinkListesi("sozcu", "http://sozcu.com.tr", finalSozcuCategoryList, "//div[@class='category-right-inner']");
            return finalSozcuCategoryList;
        }

        #endregion

        #endregion SOZCU

        //komple yapılıcak
        #region YENIAKIT

        private static List<Kategori> finalYeniAkitCategoryList = new List<Kategori>();
        private static int yeniAkitHaberSay = 0;

        public void YeniAkitHaberTara(string url, string kategoriUrl, string kategori)
        {
            try
            {
                var getHtmlWeb = new HtmlWeb();
                var document = getHtmlWeb.Load(url);
                var newAreas = document.DocumentNode.SelectNodes("//div[@class='colA']");
                if (newAreas != null)
                {
                    foreach (var newArea in newAreas)
                    {
                        HaberService HS = new HaberService();
                        try
                        {
                            string _haberKaynak = "YeniAkit";
                            string _haberKategori = kategoriUrl;
                            string _haberLink = url.ToString();
                            string _haberBaslik = document.DocumentNode.SelectSingleNode("//h1[@itemprop='headline']").InnerText;
                            string _haberBaslikSeo = DoFriendly(document.DocumentNode.SelectSingleNode("//h1[@itemprop='headline']").InnerText);

                            string _haberGorsel = "";
                            try { _haberGorsel = (document.DocumentNode.SelectSingleNode("//img[@class='image']").Attributes["src"].Value); }
                            catch { _haberGorsel = (document.DocumentNode.SelectSingleNode("//img[@itemprop='url']").Attributes["src"].Value); }

                            string _haberIcerik = Regex.Replace(Regex.Replace(document.DocumentNode.SelectSingleNode("//div[@itemprop='articleBody']").InnerHtml, "<.*?>", String.Empty), @"\s+", " ").Trim().Replace("GÜNÜN EN ÖNEMLİ MANŞETLERİ İÇİN TIKLAYIN BUGÜN NELER OLDU", "");
                            string _haberEtiket = document.DocumentNode.SelectSingleNode("//div[@class='etiketler']").InnerText;
                            string _haberOzet = "";
                            int _haberPuan = 0;
                            string _haberNot = (GoogleTrendIleKarsilastir(_haberIcerik) ? "Trend" : "");
                            DateTime _haberEklenme = DateTime.Now;

                            try { _haberEklenme = Convert.ToDateTime((document.DocumentNode.SelectSingleNode("//div[@class='date']").InnerText).Substring(document.DocumentNode.SelectSingleNode("//div[@class='date']").InnerText.Length - 16)); }
                            catch { _haberEklenme = Convert.ToDateTime((document.DocumentNode.SelectSingleNode("//div[@class='dt']").InnerText).Replace("|", "").Replace(" ", "").Replace("-", " ").Substring((document.DocumentNode.SelectSingleNode("//div[@class='dt']").InnerText).Replace("|", "").Replace(" ", "").Replace("-", " ").Length - 16)); }

                            string _haberMetaBaslik = document.DocumentNode.SelectSingleNode("//meta[@property='og:title']").Attributes["content"].Value.Replace("&#39;", "'");
                            string _haberMetaKisaBaslik = document.DocumentNode.SelectSingleNode("//meta[@property='og:description']").Attributes["content"].Value.Replace("&#39;", "'");
                            string _haberMetaGorsel = document.DocumentNode.SelectSingleNode("//meta[@property='og:image']").Attributes["content"].Value.Replace("&#39;", "'");
                            int _haberOkunmaSuresi = OkunmaSuresiHesapla(_haberIcerik);
                            string _haberKaynakAdi = "YeniAkit";
                            string _haberKategoriAdi = kategori.Replace("-", "/");

                            HS.insert(_haberKaynak, _haberKategori, _haberLink, _haberBaslik, _haberBaslikSeo, _haberGorsel, _haberIcerik, _haberEtiket, _haberOzet, _haberPuan, _haberNot, _haberMetaBaslik, _haberMetaKisaBaslik, _haberMetaGorsel, _haberEklenme, _haberOkunmaSuresi, _haberKaynakAdi, _haberKategoriAdi);
                            yeniAkitHaberSay++;
                        }
                        catch { }
                    }
                }
                else { }
            }
            catch { }
        }

        public int YeniAkitBot()
        {
            KategoriTara("http://YeniAkit.com.tr", "links", finalYeniAkitCategoryList);
            LinkTara("YeniAkit", "http://YeniAkit.com.tr", finalYeniAkitCategoryList, "colA");
            return yeniAkitHaberSay;
        }

        #endregion MILLIYET

        //komple yapılıcak
        #region FANATIK

        private static List<Kategori> finalFanatikCategoryList = new List<Kategori>();
        private static int fanatikHaberSay = 0;

        public void FanatikHaberTara(string url, string kategoriUrl, string kategori)
        {
            try
            {
                var getHtmlWeb = new HtmlWeb();
                var document = getHtmlWeb.Load(url);
                var newAreas = document.DocumentNode.SelectNodes("//div[@class='colA']");
                if (newAreas != null)
                {
                    foreach (var newArea in newAreas)
                    {
                        HaberService HS = new HaberService();
                        try
                        {
                            string _haberKaynak = "Fanatik";
                            string _haberKategori = kategoriUrl;
                            string _haberLink = url.ToString();
                            string _haberBaslik = document.DocumentNode.SelectSingleNode("//h1[@itemprop='headline']").InnerText;
                            string _haberBaslikSeo = DoFriendly(document.DocumentNode.SelectSingleNode("//h1[@itemprop='headline']").InnerText);

                            string _haberGorsel = "";
                            try { _haberGorsel = (document.DocumentNode.SelectSingleNode("//img[@class='image']").Attributes["src"].Value); }
                            catch { _haberGorsel = (document.DocumentNode.SelectSingleNode("//img[@itemprop='url']").Attributes["src"].Value); }

                            string _haberIcerik = Regex.Replace(Regex.Replace(document.DocumentNode.SelectSingleNode("//div[@itemprop='articleBody']").InnerHtml, "<.*?>", String.Empty), @"\s+", " ").Trim().Replace("GÜNÜN EN ÖNEMLİ MANŞETLERİ İÇİN TIKLAYIN BUGÜN NELER OLDU", "");
                            string _haberEtiket = document.DocumentNode.SelectSingleNode("//div[@class='etiketler']").InnerText;
                            string _haberOzet = "";
                            int _haberPuan = 0;
                            string _haberNot = (GoogleTrendIleKarsilastir(_haberIcerik) ? "Trend" : "");
                            DateTime _haberEklenme = DateTime.Now;

                            try { _haberEklenme = Convert.ToDateTime((document.DocumentNode.SelectSingleNode("//div[@class='date']").InnerText).Substring(document.DocumentNode.SelectSingleNode("//div[@class='date']").InnerText.Length - 16)); }
                            catch { _haberEklenme = Convert.ToDateTime((document.DocumentNode.SelectSingleNode("//div[@class='dt']").InnerText).Replace("|", "").Replace(" ", "").Replace("-", " ").Substring((document.DocumentNode.SelectSingleNode("//div[@class='dt']").InnerText).Replace("|", "").Replace(" ", "").Replace("-", " ").Length - 16)); }

                            string _haberMetaBaslik = document.DocumentNode.SelectSingleNode("//meta[@property='og:title']").Attributes["content"].Value.Replace("&#39;", "'");
                            string _haberMetaKisaBaslik = document.DocumentNode.SelectSingleNode("//meta[@property='og:description']").Attributes["content"].Value.Replace("&#39;", "'");
                            string _haberMetaGorsel = document.DocumentNode.SelectSingleNode("//meta[@property='og:image']").Attributes["content"].Value.Replace("&#39;", "'");
                            int _haberOkunmaSuresi = OkunmaSuresiHesapla(_haberIcerik);
                            string _haberKaynakAdi = "Fanatik";
                            string _haberKategoriAdi = kategori.Replace("-", "/");

                            HS.insert(_haberKaynak, _haberKategori, _haberLink, _haberBaslik, _haberBaslikSeo, _haberGorsel, _haberIcerik, _haberEtiket, _haberOzet, _haberPuan, _haberNot, _haberMetaBaslik, _haberMetaKisaBaslik, _haberMetaGorsel, _haberEklenme, _haberOkunmaSuresi, _haberKaynakAdi, _haberKategoriAdi);
                            fanatikHaberSay++;
                        }
                        catch { }
                    }
                }
                else { }
            }
            catch { }
        }

        public int FanatikBot()
        {
            KategoriTara("http://Fanatik.com.tr", "links", finalFanatikCategoryList);
            LinkTara("Fanatik", "http://Fanatik.com.tr", finalFanatikCategoryList, "colA");
            return fanatikHaberSay;
        }

        #endregion MILLIYET

        //komple yapılıcak
        #region FOTOMAC

        private static List<Kategori> finalFotomacCategoryList = new List<Kategori>();
        private static int fotomacHaberSay = 0;

        public void FotomacHaberTara(string url, string kategoriUrl, string kategori)
        {
            try
            {
                var getHtmlWeb = new HtmlWeb();
                var document = getHtmlWeb.Load(url);
                var newAreas = document.DocumentNode.SelectNodes("//div[@class='colA']");
                if (newAreas != null)
                {
                    foreach (var newArea in newAreas)
                    {
                        HaberService HS = new HaberService();
                        try
                        {
                            string _haberKaynak = "Fotomac";
                            string _haberKategori = kategoriUrl;
                            string _haberLink = url.ToString();
                            string _haberBaslik = document.DocumentNode.SelectSingleNode("//h1[@itemprop='headline']").InnerText;
                            string _haberBaslikSeo = DoFriendly(document.DocumentNode.SelectSingleNode("//h1[@itemprop='headline']").InnerText);

                            string _haberGorsel = "";
                            try { _haberGorsel = (document.DocumentNode.SelectSingleNode("//img[@class='image']").Attributes["src"].Value); }
                            catch { _haberGorsel = (document.DocumentNode.SelectSingleNode("//img[@itemprop='url']").Attributes["src"].Value); }

                            string _haberIcerik = Regex.Replace(Regex.Replace(document.DocumentNode.SelectSingleNode("//div[@itemprop='articleBody']").InnerHtml, "<.*?>", String.Empty), @"\s+", " ").Trim().Replace("GÜNÜN EN ÖNEMLİ MANŞETLERİ İÇİN TIKLAYIN BUGÜN NELER OLDU", "");
                            string _haberEtiket = document.DocumentNode.SelectSingleNode("//div[@class='etiketler']").InnerText;
                            string _haberOzet = "";
                            int _haberPuan = 0;
                            string _haberNot = (GoogleTrendIleKarsilastir(_haberIcerik) ? "Trend" : "");
                            DateTime _haberEklenme = DateTime.Now;

                            try { _haberEklenme = Convert.ToDateTime((document.DocumentNode.SelectSingleNode("//div[@class='date']").InnerText).Substring(document.DocumentNode.SelectSingleNode("//div[@class='date']").InnerText.Length - 16)); }
                            catch { _haberEklenme = Convert.ToDateTime((document.DocumentNode.SelectSingleNode("//div[@class='dt']").InnerText).Replace("|", "").Replace(" ", "").Replace("-", " ").Substring((document.DocumentNode.SelectSingleNode("//div[@class='dt']").InnerText).Replace("|", "").Replace(" ", "").Replace("-", " ").Length - 16)); }

                            string _haberMetaBaslik = document.DocumentNode.SelectSingleNode("//meta[@property='og:title']").Attributes["content"].Value.Replace("&#39;", "'");
                            string _haberMetaKisaBaslik = document.DocumentNode.SelectSingleNode("//meta[@property='og:description']").Attributes["content"].Value.Replace("&#39;", "'");
                            string _haberMetaGorsel = document.DocumentNode.SelectSingleNode("//meta[@property='og:image']").Attributes["content"].Value.Replace("&#39;", "'");
                            int _haberOkunmaSuresi = OkunmaSuresiHesapla(_haberIcerik);
                            string _haberKaynakAdi = "Fotomac";
                            string _haberKategoriAdi = kategori.Replace("-", "/");

                            HS.insert(_haberKaynak, _haberKategori, _haberLink, _haberBaslik, _haberBaslikSeo, _haberGorsel, _haberIcerik, _haberEtiket, _haberOzet, _haberPuan, _haberNot, _haberMetaBaslik, _haberMetaKisaBaslik, _haberMetaGorsel, _haberEklenme, _haberOkunmaSuresi, _haberKaynakAdi, _haberKategoriAdi);
                            fotomacHaberSay++;
                        }
                        catch { }
                    }
                }
                else { }
            }
            catch { }
        }

        public int FotomacBot()
        {
            KategoriTara("http://Fotomac.com.tr", "links", finalFotomacCategoryList);
            LinkTara("Fotomac", "http://Fotomac.com.tr", finalFotomacCategoryList, "colA");
            return fotomacHaberSay;
        }

        #endregion MILLIYET

        //komple yapılıcak
        #region POSTA

        private static List<Kategori> finalPostaCategoryList = new List<Kategori>();
        private static int postaHaberSay = 0;

        public void PostaHaberTara(string url, string kategoriUrl, string kategori)
        {
            try
            {
                var getHtmlWeb = new HtmlWeb();
                var document = getHtmlWeb.Load(url);
                var newAreas = document.DocumentNode.SelectNodes("//div[@class='colA']");
                if (newAreas != null)
                {
                    foreach (var newArea in newAreas)
                    {
                        HaberService HS = new HaberService();
                        try
                        {
                            string _haberKaynak = "Posta";
                            string _haberKategori = kategoriUrl;
                            string _haberLink = url.ToString();
                            string _haberBaslik = document.DocumentNode.SelectSingleNode("//h1[@itemprop='headline']").InnerText;
                            string _haberBaslikSeo = DoFriendly(document.DocumentNode.SelectSingleNode("//h1[@itemprop='headline']").InnerText);

                            string _haberGorsel = "";
                            try { _haberGorsel = (document.DocumentNode.SelectSingleNode("//img[@class='image']").Attributes["src"].Value); }
                            catch { _haberGorsel = (document.DocumentNode.SelectSingleNode("//img[@itemprop='url']").Attributes["src"].Value); }

                            string _haberIcerik = Regex.Replace(Regex.Replace(document.DocumentNode.SelectSingleNode("//div[@itemprop='articleBody']").InnerHtml, "<.*?>", String.Empty), @"\s+", " ").Trim().Replace("GÜNÜN EN ÖNEMLİ MANŞETLERİ İÇİN TIKLAYIN BUGÜN NELER OLDU", "");
                            string _haberEtiket = document.DocumentNode.SelectSingleNode("//div[@class='etiketler']").InnerText;
                            string _haberOzet = "";
                            int _haberPuan = 0;
                            string _haberNot = (GoogleTrendIleKarsilastir(_haberIcerik) ? "Trend" : "");
                            DateTime _haberEklenme = DateTime.Now;

                            try { _haberEklenme = Convert.ToDateTime((document.DocumentNode.SelectSingleNode("//div[@class='date']").InnerText).Substring(document.DocumentNode.SelectSingleNode("//div[@class='date']").InnerText.Length - 16)); }
                            catch { _haberEklenme = Convert.ToDateTime((document.DocumentNode.SelectSingleNode("//div[@class='dt']").InnerText).Replace("|", "").Replace(" ", "").Replace("-", " ").Substring((document.DocumentNode.SelectSingleNode("//div[@class='dt']").InnerText).Replace("|", "").Replace(" ", "").Replace("-", " ").Length - 16)); }

                            string _haberMetaBaslik = document.DocumentNode.SelectSingleNode("//meta[@property='og:title']").Attributes["content"].Value.Replace("&#39;", "'");
                            string _haberMetaKisaBaslik = document.DocumentNode.SelectSingleNode("//meta[@property='og:description']").Attributes["content"].Value.Replace("&#39;", "'");
                            string _haberMetaGorsel = document.DocumentNode.SelectSingleNode("//meta[@property='og:image']").Attributes["content"].Value.Replace("&#39;", "'");
                            int _haberOkunmaSuresi = OkunmaSuresiHesapla(_haberIcerik);
                            string _haberKaynakAdi = "Posta";
                            string _haberKategoriAdi = kategori.Replace("-", "/");

                            HS.insert(_haberKaynak, _haberKategori, _haberLink, _haberBaslik, _haberBaslikSeo, _haberGorsel, _haberIcerik, _haberEtiket, _haberOzet, _haberPuan, _haberNot, _haberMetaBaslik, _haberMetaKisaBaslik, _haberMetaGorsel, _haberEklenme, _haberOkunmaSuresi, _haberKaynakAdi, _haberKategoriAdi);
                            postaHaberSay++;
                        }
                        catch { }
                    }
                }
                else { }
            }
            catch { }
        }

        public int PostaBot()
        {
            KategoriTara("http://Posta.com.tr", "links", finalPostaCategoryList);
            LinkTara("Posta", "http://Posta.com.tr", finalPostaCategoryList, "colA");
            return postaHaberSay;
        }

        #endregion MILLIYET

        #region HELPER

        //dakikada 120 kelime okumaya göre okuma süresi hesaplar
        public int OkunmaSuresiHesapla(string fullText)
        {
            fullText = Regex.Replace(fullText, @"\s{2,}", " ");
            double wordCount = fullText.Split(' ').Length;
            return Convert.ToInt32(Math.Ceiling(wordCount / 120));
        }

        //gereksiz kelimeler ön plana çıkıyo. şimdilik kullanışsız
        public List<string> OtomatikEtiketOlustur(string fullText)
        {
            var list = new List<string>();
            string[] values = Regex.Replace(fullText, @"\s{2,}", " ").Split(' ');
            foreach (string value in values)
            {
                list.Add(value);
            }

            var q = list.GroupBy(x => x)
                        .Select(g => new { Value = g.Key, Count = g.Count() })
                        .Where(g => g.Count >= 2)
                        .OrderByDescending(x => x.Count)
                        .Select(g => g.Value);

            return q.ToList();
        }

        //url oluştur
        public string DoFriendly(string txt)
        {
            txt = txt.Replace('ı', 'i');
            txt = txt.Replace('ö', 'o');
            txt = txt.Replace('ü', 'u');
            txt = txt.Replace('ş', 's');
            txt = txt.Replace('ğ', 'g');
            txt = txt.Replace('ç', 'c');
            txt = txt.Replace('İ', 'I');
            txt = txt.Replace('Ö', 'O');
            txt = txt.Replace('Ü', 'U');
            txt = txt.Replace('Ş', 'S');
            txt = txt.Replace('Ğ', 'G');
            txt = txt.Replace('Ç', 'C');

            byte[] bytes = Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            string str = Encoding.ASCII.GetString(bytes).ToLower();

            if (str.Length >= 50)
            {
                str = str.Substring(0, 50);
            }

            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            str = Regex.Replace(str, @"\s+", " ").Trim();
            str = Regex.Replace(str, @"\s", "-");
            return str;
        }

        //link taranmışsa yada yasaklı içeriyosa FALSE, temizse TRUE
        public bool CheckBlackList(string url, List<string> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                bool result = url.Contains(list[i].ToString());
                if (result) { return false; }
            }
            return true;
        }

        //sitedeki menüyü bul ve kategorileri tara
        public void KategoriTara(string site, string menuDiv, List<Kategori> siteList)
        {
            try
            {
                HtmlWeb hw = new HtmlWeb();
                HtmlDocument doc = new HtmlDocument();
                doc = hw.Load(site);
                foreach (HtmlNode linkDiv in doc.DocumentNode.SelectNodes(menuDiv))
                {
                    var links = linkDiv.Descendants("a").ToList();
                    foreach (var item in links)
                    {
                        switch (site)
                        {
                            case "http://sozcu.com.tr":

                                if (item.Attributes["href"] != null && item.Attributes["href"].Value != "/" && item.Attributes["href"].Value.Length > 1)
                                {
                                    siteList.Add(new Kategori
                                    {
                                        KategoriURL = item.Attributes["href"].Value,
                                        KategoriAdi = KategoriAdiniTemizle(item.InnerHtml)
                                    });
                                }

                                break;
                            default:

                                if (item.Attributes["href"] != null && item.Attributes["href"].Value != null && item.Attributes["href"].Value != "/" && item.Attributes["href"].Value.Substring(0, 1) == "/" && item.Attributes["href"].Value.Length > 1)
                                {
                                    siteList.Add(new Kategori
                                    {
                                        KategoriURL = item.Attributes["href"].Value,
                                        KategoriAdi = KategoriAdiniTemizle(item.InnerHtml)
                                    });
                                }

                                break;
                        }
                    }
                }
            }
            catch (Exception ex) { var x = ex; var xx = ex.Message; }
        }

        //forma kategori sayfalarının url'lerini yollayan servis
        //Split(',') metodu ile ayrılması gerekiyo
        public void KategoriListesi(string site, string menuDiv, List<Kategori> siteList)
        {
            try
            {
                HtmlWeb hw = new HtmlWeb();
                HtmlDocument doc = new HtmlDocument();
                doc = hw.Load(site);
                foreach (HtmlNode linkDiv in doc.DocumentNode.SelectNodes(menuDiv))
                {
                    var links = linkDiv.Descendants("a").ToList();
                    foreach (var item in links)
                    {
                        switch (site)
                        {
                            case "http://sozcu.com.tr":

                                if (item.Attributes["href"] != null && item.Attributes["href"].Value != "/" && item.Attributes["href"].Value.Length > 1)
                                {
                                    siteList.Add(new Kategori
                                    {
                                        KategoriURL = item.Attributes["href"].Value,
                                        KategoriAdi = KategoriAdiniTemizle(item.InnerHtml)
                                    });
                                }

                                break;
                            default:

                                if (item.Attributes["href"] != null && item.Attributes["href"].Value != null && item.Attributes["href"].Value != "/" && item.Attributes["href"].Value.Substring(0, 1) == "/" && item.Attributes["href"].Value.Length > 1)
                                {
                                    siteList.Add(new Kategori
                                    {
                                        KategoriURL = item.Attributes["href"].Value,
                                        KategoriAdi = KategoriAdiniTemizle(item.InnerHtml)
                                    });
                                }

                                break;
                        }
                    }
                }
            }
            catch (Exception ex) { var x = ex; var xx = ex.Message; }
        }

        //linktara
        public void LinkTara(string site, string url, List<Kategori> siteList, string haberArea)
        {
            try
            {
                foreach (var item in siteList)
                {
                    try
                    {
                        HtmlWeb hw = new HtmlWeb();
                        HtmlDocument doc = new HtmlDocument();
                        doc = hw.Load((site == "sozcu" ? item.KategoriURL : url + item.KategoriURL));
                        //tüm linkleri bul
                        foreach (HtmlNode link in doc.DocumentNode.SelectNodes(haberArea))
                        {
                            try
                            {
                                var links = link.Descendants("a").ToList();
                                foreach (var item2 in links)
                                {
                                    switch (site)
                                    {
                                        case "sabah":
                                            if (item2.Attributes["href"].Value != "/" && item2.Attributes["href"].Value.Substring(0, 1) == "/" && item2.Attributes["href"].Value.Length > 1 && item2.Attributes["href"].Value.Contains(item.KategoriURL))
                                            {
                                                SabahHaberTara(url + item2.Attributes["href"].Value, item.KategoriAdi);
                                            }
                                            break;
                                        case "milliyet":
                                            if (!item.KategoriURL.Contains("sondakika"))
                                            {
                                                MilliyetHaberTara(item2.Attributes["href"].Value, item.KategoriURL.Replace("/", ""), item.KategoriAdi);
                                            }
                                            break;
                                        //case "hurriyet":
                                        //    if (item2.Attributes["href"].Value != "/" && item2.Attributes["href"].Value.Substring(0, 1) == "/" && item2.Attributes["href"].Value.Length > 1 && item2.Attributes["href"].Value != item.KategoriURL && !item.KategoriURL.Contains("son-dakika"))
                                        //    {
                                        //        HurriyetHaberTara(url + item2.Attributes["href"].Value, item.KategoriURL.Replace("/", ""), item.KategoriAdi);
                                        //    }
                                        //    break;
                                        //case "sozcu":
                                        //    if (item2.Attributes["href"].Value != "/" && item2.Attributes["href"].Value.Length > 1 && item2.Attributes["href"].Value != item.KategoriURL && !item.KategoriURL.Contains("son-dakika"))
                                        //    {
                                        //        SozcuHaberTara(item2.Attributes["href"].Value, item.KategoriURL.Replace("/", ""), item.KategoriAdi);
                                        //    }
                                        //    break;
                                        default:
                                            Console.WriteLine("Default case");
                                            break;
                                    }
                                }
                            }
                            catch (Exception ex) { var x = ex; var xx = ex.Message; }
                        }
                    }
                    catch (Exception ex) { var x = ex; var xx = ex.Message; }
                }
            }
            catch (Exception ex) { var x = ex; var xx = ex.Message; }
        }

        //formda data gelmesi için forma url list yollayan servis
        //Split(',') metodu ile ayrılması gerekiyo
        public List<string> LinkListesi(string site, string url, List<Kategori> siteList, string haberArea)
        {
            List<string> ss = new List<string>();
            try
            {
                foreach (var item in siteList)
                {
                    try
                    {
                        HtmlWeb hw = new HtmlWeb();
                        HtmlDocument doc = new HtmlDocument();
                        doc = hw.Load((site == "sozcu" ? item.KategoriURL : url + item.KategoriURL));
                        //tüm linkleri bul
                        foreach (HtmlNode link in doc.DocumentNode.SelectNodes(haberArea))
                        {
                            try
                            {
                                var links = link.Descendants("a").ToList();
                                foreach (var item2 in links)
                                {
                                    switch (site)
                                    {
                                        //case "sabah":
                                        //    if (item2.Attributes["href"].Value != "/" && item2.Attributes["href"].Value.Substring(0, 1) == "/" && item2.Attributes["href"].Value.Length > 1 && item2.Attributes["href"].Value.Contains(item.KategoriURL))
                                        //    {
                                        //        ss.Add(url + item2.Attributes["href"].Value + "," + item.KategoriAdi);
                                        //    }
                                        //    break;
                                        //case "milliyet":
                                        //    if (!item.KategoriURL.Contains("sondakika"))
                                        //    {
                                        //        ss.Add(url + item2.Attributes["href"].Value + "," + item.KategoriURL.Replace("/", "") + "," + item.KategoriAdi);
                                        //    }
                                        //    break;
                                        case "hurriyet":
                                            if (item2.Attributes["href"].Value != "/" && item2.Attributes["href"].Value.Substring(0, 1) == "/" && item2.Attributes["href"].Value.Length > 1 && item2.Attributes["href"].Value != item.KategoriURL && !item.KategoriURL.Contains("son-dakika"))
                                            {
                                                ss.Add(url + item2.Attributes["href"].Value + "," + item.KategoriURL.Replace("/", "") + "," + item.KategoriAdi);
                                            }
                                            break;
                                        case "sozcu":
                                            if (item2.Attributes["href"].Value != "/" && item2.Attributes["href"].Value.Length > 1 && item2.Attributes["href"].Value != item.KategoriURL && !item.KategoriURL.Contains("son-dakika"))
                                            {
                                                ss.Add(url + item2.Attributes["href"].Value + "," + item.KategoriURL.Replace("/", "") + "," + item.KategoriAdi);
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                            catch (Exception ex) { var x = ex; var xx = ex.Message; }
                        }
                    }
                    catch (Exception ex) { var x = ex; var xx = ex.Message; }
                }
            }
            catch (Exception ex) { var x = ex; var xx = ex.Message; }

            return ss;
        }

        public List<string> LinkListesiForAngular(string site, string url, Kategori kategori, string doc, string haberArea)
        {
            List<string> ss = new List<string>();
            try
            {
                HtmlDocument document = new HtmlDocument();
                document.LoadHtml(doc);
                var newAreas = document.DocumentNode.SelectNodes(haberArea);
                if (newAreas != null)
                {
                    foreach (var newArea in newAreas)
                    {
                        HaberService HS = new HaberService();
                        try
                        {
                            var links = newArea.Descendants("a").ToList();
                            foreach (var item2 in links)
                            {
                                switch (site)
                                {
                                    case "hurriyet":
                                        if (item2.Attributes["href"].Value != "/" && item2.Attributes["href"].Value.Substring(0, 1) == "/" && item2.Attributes["href"].Value.Length > 1 && item2.Attributes["href"].Value != kategori.KategoriURL && !kategori.KategoriURL.Contains("son-dakika"))
                                        {
                                            ss.Add(url + item2.Attributes["href"].Value + "," + kategori.KategoriURL + "," + kategori.KategoriAdi);
                                        }
                                        break;
                                    case "sozcu":
                                        if (item2.Attributes["href"].Value != "/" && item2.Attributes["href"].Value.Length > 1 && item2.Attributes["href"].Value != kategori.KategoriURL && !kategori.KategoriURL.Contains("son-dakika"))
                                        {
                                            ss.Add(url + item2.Attributes["href"].Value + "," + kategori.KategoriURL + "," + kategori.KategoriAdi);
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        catch (Exception ex) { var x = ex; var xx = ex.Message; }
                    }
                }
                else { }
            }
            catch { }

            return ss;
        }

        //google dom ile ekleme yaptığı için şimdilik geçersiz
        public bool GoogleTrendIleKarsilastir(string fullText)
        {
            List<string> trendList = new List<string>();
            trendList.Add("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");

            for (int i = 0; i < trendList.Count; i++)
            {
                if (fullText.Contains(trendList[i].ToString()))
                {
                    return true;
                }
            }
            return false;
        }

        //kategori adlarını temizle
        public string KategoriAdiniTemizle(string txt)
        {
            txt = txt.Replace("-", "/");
            txt = txt.Replace("&#304;", "İ");
            txt = txt.Replace("&#305;", "ı");
            txt = txt.Replace("&#214;", "Ö");
            txt = txt.Replace("&#246;", "ö");
            txt = txt.Replace("&#220;", "Ü");
            txt = txt.Replace("&#252;", "ü");
            txt = txt.Replace("&#199;", "Ç");
            txt = txt.Replace("&#231;", "ç");
            txt = txt.Replace("&#286;", "Ğ");
            txt = txt.Replace("&#287;", "ğ");
            txt = txt.Replace("&#350;", "Ş");
            txt = txt.Replace("&#351;", "ş");
            return txt;
        }

        #endregion

    }
}