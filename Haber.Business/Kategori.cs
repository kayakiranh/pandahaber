﻿namespace Haber.Business
{
    public class Kategori
    {
        public string KategoriURL { get; set; }
        public string KategoriAdi { get; set; }
    }
}