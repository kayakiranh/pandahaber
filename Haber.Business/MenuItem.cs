﻿using System.Collections.Generic;

namespace Haber.Business
{
    public class MenuItem
    {
        public string MenuKaynak { get; set; }
        public string MenuKaynakAdi { get; set; }
        public List<Kategori> MenuKategori { get; set; }
    }
}