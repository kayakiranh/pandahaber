﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Haber.Business
{
    public class HaberService
    {
        private static string connectionString = "Data Source=SQL5014.SmarterASP.NET;Initial Catalog=DB_9ADF55_hbr;User Id=DB_9ADF55_hbr_admin;Password=HbR123321rBh;";
        private static string selectQuery = "SELECT COUNT(*) FROM Haberler WHERE HaberLink = @_haberLink AND HaberKategori = @_haberKategori";
        private static string insertQuery = "INSERT INTO Haberler (HaberKaynak ,HaberKategori ,HaberLink ,HaberBaslik ,HaberBaslikSeo ,HaberGorsel ,HaberIcerik ,HaberEtiket ,HaberOzet ,HaberPuan ,HaberNot ,HaberOkunma ,HaberEklenme ,HaberDurum ,HaberMetaBaslik ,HaberMetaKisaBaslik ,HaberMetaGorsel, HaberOkunmaSuresi, HaberKaynakAdi, HaberKategoriAdi, HaberOlusturulma) VALUES (@HaberKaynak , @HaberKategori , @HaberLink , @HaberBaslik , @HaberBaslikSeo , @HaberGorsel , @HaberIcerik , @HaberEtiket , @HaberOzet , @HaberPuan , @HaberNot , @HaberOkunma , @HaberEklenme , @HaberDurum , @HaberMetaBaslik , @HaberMetaKisaBaslik , @HaberMetaGorsel, @HaberOkunmaSuresi, @HaberKaynakAdi, @HaberKategoriAdi, @HaberOlusturulma)";
        private static string updateCountQuery = "UPDATE Haberler SET HaberOkunma = HaberOkunma + 1 WHERE HaberID = @id";
        private static string clearBeforeRemoveQuery = "SET IDENTITY_INSERT EskiHaberler ON INSERT INTO EskiHaberler (EskiHaberID ,EskiHaberKaynak ,EskiHaberKategori ,EskiHaberLink ,EskiHaberBaslik ,EskiHaberBaslikSeo ,EskiHaberGorsel ,EskiHaberIcerik ,EskiHaberEtiket ,EskiHaberOzet ,EskiHaberPuan ,EskiHaberNot ,EskiHaberOkunma ,EskiHaberEklenme ,EskiHaberDurum ,EskiHaberMetaBaslik ,EskiHaberMetaKisaBaslik ,EskiHaberMetaGorsel ,EskiHaberOkunmaSuresi ,EskiHaberKaynakAdi ,EskiHaberKategoriAdi, EskiHaberOlusturulma) SELECT HaberID ,HaberKaynak ,HaberKategori ,HaberLink ,HaberBaslik ,HaberBaslikSeo ,HaberGorsel ,HaberIcerik ,HaberEtiket ,HaberOzet ,HaberPuan ,HaberNot ,HaberOkunma ,HaberEklenme ,HaberDurum ,HaberMetaBaslik ,HaberMetaKisaBaslik ,HaberMetaGorsel ,HaberOkunmaSuresi ,HaberKaynakAdi ,HaberKategoriAdi, HaberOlusturulma FROM Haberler WHERE HaberOlusturulma < DATEADD(day,-1,GETDATE()) SET IDENTITY_INSERT EskiHaberler OFF";
        private static string removeAfterClearQuery = "DELETE FROM Haberler WHERE HaberOlusturulma < DATEADD(day,-1,GETDATE())";
        private static string truncateHaberler = "TRUNCATE TABLE Haberler";

        public string insert(string _haberKaynak, string _haberKategori, string _haberLink, string _haberBaslik, string _haberBaslikSeo, string _haberGorsel, string _haberIcerik, string _haberEtiket, string _haberOzet, int _haberPuan, string _haberNot, string _haberMetaBaslik, string _haberMetaKisaBaslik, string _haberMetaGorsel, DateTime _haberEklenme, int _haberOkunmaSuresi, string _haberKaynakAdi, string _haberKategoriAdi)
        {
            try
            {
                int counter = 0;
                using (SqlConnection cn = new SqlConnection(connectionString))
                using (SqlCommand cmd = new SqlCommand(selectQuery, cn))
                {
                    cmd.Parameters.AddWithValue("@_haberLink", _haberLink);
                    cmd.Parameters.AddWithValue("@_haberKategori", _haberKategori);

                    cn.Open();
                    counter = (int)cmd.ExecuteScalar();
                    cn.Close();
                }

                if (counter == 0)
                {
                    using (SqlConnection cn2 = new SqlConnection(connectionString))
                    using (SqlCommand cmd2 = new SqlCommand(insertQuery, cn2))
                    {
                        cmd2.Parameters.AddWithValue("@HaberKaynak", _haberKaynak);
                        cmd2.Parameters.AddWithValue("@HaberKategori", _haberKategori);
                        cmd2.Parameters.AddWithValue("@HaberLink", _haberLink);
                        cmd2.Parameters.AddWithValue("@HaberBaslik", _haberBaslik);
                        cmd2.Parameters.AddWithValue("@HaberBaslikSeo", _haberBaslikSeo);
                        cmd2.Parameters.AddWithValue("@HaberGorsel", _haberGorsel);
                        cmd2.Parameters.AddWithValue("@HaberIcerik", _haberIcerik);
                        cmd2.Parameters.AddWithValue("@HaberEtiket", _haberEtiket);
                        cmd2.Parameters.AddWithValue("@HaberOzet", _haberOzet);
                        cmd2.Parameters.AddWithValue("@HaberPuan", _haberPuan);
                        cmd2.Parameters.AddWithValue("@HaberNot", _haberNot);
                        cmd2.Parameters.AddWithValue("@HaberOkunma", 0);
                        cmd2.Parameters.AddWithValue("@HaberEklenme", _haberEklenme);
                        cmd2.Parameters.AddWithValue("@HaberDurum", true);
                        cmd2.Parameters.AddWithValue("@HaberMetaBaslik", _haberMetaBaslik);
                        cmd2.Parameters.AddWithValue("@HaberMetaKisaBaslik", _haberMetaKisaBaslik);
                        cmd2.Parameters.AddWithValue("@HaberMetaGorsel", _haberMetaGorsel);
                        cmd2.Parameters.AddWithValue("@HaberOkunmaSuresi", _haberOkunmaSuresi);
                        cmd2.Parameters.AddWithValue("@HaberKaynakAdi", _haberKaynakAdi);
                        cmd2.Parameters.AddWithValue("@HaberKategoriAdi", _haberKategoriAdi);
                        cmd2.Parameters.AddWithValue("@HaberOlusturulma", DateTime.Now);

                        cn2.Open();
                        cmd2.ExecuteNonQuery();
                        cn2.Close();
                    }
                    return "INSERT : OK";
                }
                else
                {
                    return "INSERT : EXIST";
                }
            }
            catch (Exception ex) { var x = ex; var xx = ex.Message; return "INSERT : NOK"; }
        }

        public string clear()
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connectionString))
                using (SqlCommand cmd = new SqlCommand(clearBeforeRemoveQuery, cn))
                {
                    cn.Open();
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }

                using (SqlConnection cn = new SqlConnection(connectionString))
                using (SqlCommand cmd = new SqlCommand(removeAfterClearQuery, cn))
                {
                    cn.Open();
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }

                return "CLEAR : OK";
            }
            catch (Exception ex) { var x = ex; var xx = ex.Message; return "CLEAR : NOT OK"; }
        }

        public string truncate()
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connectionString))
                using (SqlCommand cmd = new SqlCommand(truncateHaberler, cn))
                {
                    cn.Open();
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }

                return "TRUNCATE : OK";
            }
            catch (Exception ex) { var x = ex; var xx = ex.Message; return "TRUNCATE : NOT OK"; }
        }

        public List<Haber> list(string kategori, string kaynak, int miktar)
        {
            List<Haber> list = new List<Haber>();

            try
            {
                if (kategori.ToLower() == "undefined" || kategori.ToLower() == "default" || kategori.ToLower() == "index")
                {
                    kategori = "";
                }
                if (kaynak.ToLower() == "undefined" || kaynak.ToLower() == "default" || kaynak.ToLower() == "index")
                {
                    kaynak = "";
                }

                if (!string.IsNullOrEmpty(kategori) && kaynak != "tumu" && !string.IsNullOrEmpty(kaynak))
                {
                    using (SqlConnection cn = new SqlConnection(connectionString))
                    using (SqlCommand cmd = new SqlCommand("SELECT TOP (@_miktar) * FROM Haberler WHERE HaberKategori = @_kategori AND HaberKaynak = @_kaynak ORDER BY HaberEklenme DESC", cn))
                    {
                        cmd.Parameters.AddWithValue("@_miktar", (miktar == 0 ? 1000 : miktar));
                        cmd.Parameters.AddWithValue("@_kategori", kategori);
                        cmd.Parameters.AddWithValue("@_kaynak", kaynak);

                        cn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            list.Add(new Haber
                            {
                                HaberBaslik = reader["HaberBaslik"].ToString(),
                                HaberBaslikSeo = reader["HaberBaslikSeo"].ToString(),
                                HaberDurum = Convert.ToBoolean(reader["HaberDurum"].ToString()),
                                HaberEklenme = Convert.ToDateTime(reader["HaberEklenme"].ToString()),
                                HaberEtiket = reader["HaberEtiket"].ToString(),
                                HaberGorsel = reader["HaberGorsel"].ToString(),
                                HaberIcerik = reader["HaberIcerik"].ToString(),
                                HaberID = Convert.ToInt32(reader["HaberID"].ToString()),
                                HaberKategori = reader["HaberKategori"].ToString(),
                                HaberKaynak = reader["HaberKaynak"].ToString(),
                                HaberLink = reader["HaberLink"].ToString(),
                                HaberNot = reader["HaberNot"].ToString(),
                                HaberOkunma = Convert.ToInt32(reader["HaberOkunma"].ToString()),
                                HaberOzet = reader["HaberOzet"].ToString(),
                                HaberPuan = Convert.ToInt32(reader["HaberPuan"].ToString()),
                                HaberMetaBaslik = reader["HaberMetaBaslik"].ToString(),
                                HaberMetaKisaBaslik = reader["HaberMetaKisaBaslik"].ToString(),
                                HaberMetaGorsel = reader["HaberMetaGorsel"].ToString(),
                                HaberOkunmaSuresi = Convert.ToInt32(reader["HaberOkunmaSuresi"].ToString()),
                                HaberKaynakAdi = reader["HaberKaynakAdi"].ToString(),
                                HaberKategoriAdi = reader["HaberKategoriAdi"].ToString(),
                                HaberOlusturulma = Convert.ToDateTime(reader["HaberOlusturulma"].ToString())
                            });
                        }

                        reader.Close();
                        cn.Close();
                    }
                }
                else if (!string.IsNullOrEmpty(kategori))
                {
                    using (SqlConnection cn = new SqlConnection(connectionString))
                    using (SqlCommand cmd = new SqlCommand("SELECT TOP (@_miktar) * FROM Haberler WHERE HaberKategori = @_kategori ORDER BY HaberEklenme DESC", cn))
                    {
                        cmd.Parameters.AddWithValue("@_miktar", (miktar == 0 ? 1000 : miktar));
                        cmd.Parameters.AddWithValue("@_kategori", kategori);

                        cn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            list.Add(new Haber
                            {
                                HaberBaslik = reader["HaberBaslik"].ToString(),
                                HaberBaslikSeo = reader["HaberBaslikSeo"].ToString(),
                                HaberDurum = Convert.ToBoolean(reader["HaberDurum"].ToString()),
                                HaberEklenme = Convert.ToDateTime(reader["HaberEklenme"].ToString()),
                                HaberEtiket = reader["HaberEtiket"].ToString(),
                                HaberGorsel = reader["HaberGorsel"].ToString(),
                                HaberIcerik = reader["HaberIcerik"].ToString(),
                                HaberID = Convert.ToInt32(reader["HaberID"].ToString()),
                                HaberKategori = reader["HaberKategori"].ToString(),
                                HaberKaynak = reader["HaberKaynak"].ToString(),
                                HaberLink = reader["HaberLink"].ToString(),
                                HaberNot = reader["HaberNot"].ToString(),
                                HaberOkunma = Convert.ToInt32(reader["HaberOkunma"].ToString()),
                                HaberOzet = reader["HaberOzet"].ToString(),
                                HaberPuan = Convert.ToInt32(reader["HaberPuan"].ToString()),
                                HaberMetaBaslik = reader["HaberMetaBaslik"].ToString(),
                                HaberMetaKisaBaslik = reader["HaberMetaKisaBaslik"].ToString(),
                                HaberMetaGorsel = reader["HaberMetaGorsel"].ToString(),
                                HaberOkunmaSuresi = Convert.ToInt32(reader["HaberOkunmaSuresi"].ToString()),
                                HaberKaynakAdi = reader["HaberKaynakAdi"].ToString(),
                                HaberKategoriAdi = reader["HaberKategoriAdi"].ToString(),
                                HaberOlusturulma = Convert.ToDateTime(reader["HaberOlusturulma"].ToString())
                            });
                        }

                        reader.Close();
                        cn.Close();
                    }
                }
                else if (!string.IsNullOrEmpty(kaynak) && kaynak != "tumu")
                {
                    using (SqlConnection cn = new SqlConnection(connectionString))
                    using (SqlCommand cmd = new SqlCommand("SELECT TOP (@_miktar) * FROM Haberler WHERE HaberKaynak = @_kaynak ORDER BY HaberEklenme DESC", cn))
                    {
                        cmd.Parameters.AddWithValue("@_miktar", (miktar == 0 ? 1000 : miktar));
                        cmd.Parameters.AddWithValue("@_kaynak", kaynak);

                        cn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            list.Add(new Haber
                            {
                                HaberBaslik = reader["HaberBaslik"].ToString(),
                                HaberBaslikSeo = reader["HaberBaslikSeo"].ToString(),
                                HaberDurum = Convert.ToBoolean(reader["HaberDurum"].ToString()),
                                HaberEklenme = Convert.ToDateTime(reader["HaberEklenme"].ToString()),
                                HaberEtiket = reader["HaberEtiket"].ToString(),
                                HaberGorsel = reader["HaberGorsel"].ToString(),
                                HaberIcerik = reader["HaberIcerik"].ToString(),
                                HaberID = Convert.ToInt32(reader["HaberID"].ToString()),
                                HaberKategori = reader["HaberKategori"].ToString(),
                                HaberKaynak = reader["HaberKaynak"].ToString(),
                                HaberLink = reader["HaberLink"].ToString(),
                                HaberNot = reader["HaberNot"].ToString(),
                                HaberOkunma = Convert.ToInt32(reader["HaberOkunma"].ToString()),
                                HaberOzet = reader["HaberOzet"].ToString(),
                                HaberPuan = Convert.ToInt32(reader["HaberPuan"].ToString()),
                                HaberMetaBaslik = reader["HaberMetaBaslik"].ToString(),
                                HaberMetaKisaBaslik = reader["HaberMetaKisaBaslik"].ToString(),
                                HaberMetaGorsel = reader["HaberMetaGorsel"].ToString(),
                                HaberOkunmaSuresi = Convert.ToInt32(reader["HaberOkunmaSuresi"].ToString()),
                                HaberKaynakAdi = reader["HaberKaynakAdi"].ToString(),
                                HaberKategoriAdi = reader["HaberKategoriAdi"].ToString(),
                                HaberOlusturulma = Convert.ToDateTime(reader["HaberOlusturulma"].ToString())
                            });
                        }

                        reader.Close();
                        cn.Close();
                    }
                }
                else
                {
                    using (SqlConnection cn = new SqlConnection(connectionString))
                    using (SqlCommand cmd = new SqlCommand("SELECT TOP (@_miktar) * FROM Haberler ORDER BY HaberEklenme DESC", cn))
                    {
                        cmd.Parameters.AddWithValue("@_miktar", (miktar == 0 ? 1000 : miktar));

                        cn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            list.Add(new Haber
                            {
                                HaberBaslik = reader["HaberBaslik"].ToString(),
                                HaberBaslikSeo = reader["HaberBaslikSeo"].ToString(),
                                HaberDurum = Convert.ToBoolean(reader["HaberDurum"].ToString()),
                                HaberEklenme = Convert.ToDateTime(reader["HaberEklenme"].ToString()),
                                HaberEtiket = reader["HaberEtiket"].ToString(),
                                HaberGorsel = reader["HaberGorsel"].ToString(),
                                HaberIcerik = reader["HaberIcerik"].ToString(),
                                HaberID = Convert.ToInt32(reader["HaberID"].ToString()),
                                HaberKategori = reader["HaberKategori"].ToString(),
                                HaberKaynak = reader["HaberKaynak"].ToString(),
                                HaberLink = reader["HaberLink"].ToString(),
                                HaberNot = reader["HaberNot"].ToString(),
                                HaberOkunma = Convert.ToInt32(reader["HaberOkunma"].ToString()),
                                HaberOzet = reader["HaberOzet"].ToString(),
                                HaberPuan = Convert.ToInt32(reader["HaberPuan"].ToString()),
                                HaberMetaBaslik = reader["HaberMetaBaslik"].ToString(),
                                HaberMetaKisaBaslik = reader["HaberMetaKisaBaslik"].ToString(),
                                HaberMetaGorsel = reader["HaberMetaGorsel"].ToString(),
                                HaberOkunmaSuresi = Convert.ToInt32(reader["HaberOkunmaSuresi"].ToString()),
                                HaberKaynakAdi = reader["HaberKaynakAdi"].ToString(),
                                HaberKategoriAdi = reader["HaberKategoriAdi"].ToString(),
                                HaberOlusturulma = Convert.ToDateTime(reader["HaberOlusturulma"].ToString())
                            });
                        }

                        reader.Close();
                        cn.Close();
                    }
                }
            }
            catch (Exception ex) { var x = ex; var xx = ex.Message; }

            return list;
        }

        public List<Haber> listMain(string kategori, string kaynak, int miktar)
        {
            List<Haber> list = new List<Haber>();

            try
            {
                if (kategori.ToLower() == "undefined" || kategori.ToLower() == "default" || kategori.ToLower() == "index")
                {
                    kategori = "";
                }
                if (kaynak.ToLower() == "undefined" || kaynak.ToLower() == "default" || kaynak.ToLower() == "index")
                {
                    kaynak = "";
                }

                if (!string.IsNullOrEmpty(kategori) && kaynak != "tumu" && !string.IsNullOrEmpty(kaynak))
                {
                    using (SqlConnection cn = new SqlConnection(connectionString))
                    using (SqlCommand cmd = new SqlCommand("SELECT TOP (@_miktar) HaberBaslik, HaberBaslikSeo, HaberEklenme, HaberGorsel, HaberKategori, HaberKaynak, HaberLink, HaberOkunma, HaberMetaBaslik, HaberMetaKisaBaslik, HaberOkunmaSuresi FROM Haberler WHERE HaberKategori = @_kategori AND HaberKaynak = @_kaynak ORDER BY HaberEklenme DESC", cn))
                    {
                        cmd.Parameters.AddWithValue("@_miktar", (miktar == 0 ? 1000 : miktar));
                        cmd.Parameters.AddWithValue("@_kategori", kategori);
                        cmd.Parameters.AddWithValue("@_kaynak", kaynak);

                        cn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            list.Add(new Haber
                            {
                                HaberBaslik = reader["HaberBaslik"].ToString(),
                                HaberBaslikSeo = reader["HaberBaslikSeo"].ToString(),
                                //HaberDurum = Convert.ToBoolean(reader["HaberDurum"].ToString()),
                                HaberEklenme = Convert.ToDateTime(reader["HaberEklenme"].ToString()),
                                //HaberEtiket = reader["HaberEtiket"].ToString(),
                                HaberGorsel = reader["HaberGorsel"].ToString(),
                                //HaberIcerik = reader["HaberIcerik"].ToString(),
                                //HaberID = Convert.ToInt32(reader["HaberID"].ToString()),
                                HaberKategori = reader["HaberKategori"].ToString(),
                                HaberKaynak = reader["HaberKaynak"].ToString(),
                                HaberLink = reader["HaberLink"].ToString(),
                                //HaberNot = reader["HaberNot"].ToString(),
                                HaberOkunma = Convert.ToInt32(reader["HaberOkunma"].ToString()),
                                //HaberOzet = reader["HaberOzet"].ToString(),
                                //HaberPuan = Convert.ToInt32(reader["HaberPuan"].ToString()),
                                HaberMetaBaslik = reader["HaberMetaBaslik"].ToString(),
                                HaberMetaKisaBaslik = reader["HaberMetaKisaBaslik"].ToString(),
                                //HaberMetaGorsel = reader["HaberMetaGorsel"].ToString(),
                                HaberOkunmaSuresi = Convert.ToInt32(reader["HaberOkunmaSuresi"].ToString())
                                //HaberKaynakAdi = reader["HaberKaynakAdi"].ToString(),
                                //HaberKategoriAdi = reader["HaberKategoriAdi"].ToString(),
                                //HaberOlusturulma = Convert.ToDateTime(reader["HaberOlusturulma"].ToString())
                            });
                        }

                        reader.Close();
                        cn.Close();
                    }
                }
                else if (!string.IsNullOrEmpty(kategori))
                {
                    using (SqlConnection cn = new SqlConnection(connectionString))
                    using (SqlCommand cmd = new SqlCommand("SELECT TOP (@_miktar) HaberBaslik, HaberBaslikSeo, HaberEklenme, HaberGorsel, HaberKategori, HaberKaynak, HaberLink, HaberOkunma, HaberMetaBaslik, HaberMetaKisaBaslik, HaberOkunmaSuresi FROM Haberler WHERE HaberKategori = @_kategori ORDER BY HaberEklenme DESC", cn))
                    {
                        cmd.Parameters.AddWithValue("@_miktar", (miktar == 0 ? 1000 : miktar));
                        cmd.Parameters.AddWithValue("@_kategori", kategori);

                        cn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            list.Add(new Haber
                            {
                                HaberBaslik = reader["HaberBaslik"].ToString(),
                                HaberBaslikSeo = reader["HaberBaslikSeo"].ToString(),
                                //HaberDurum = Convert.ToBoolean(reader["HaberDurum"].ToString()),
                                HaberEklenme = Convert.ToDateTime(reader["HaberEklenme"].ToString()),
                                //HaberEtiket = reader["HaberEtiket"].ToString(),
                                HaberGorsel = reader["HaberGorsel"].ToString(),
                                //HaberIcerik = reader["HaberIcerik"].ToString(),
                                //HaberID = Convert.ToInt32(reader["HaberID"].ToString()),
                                HaberKategori = reader["HaberKategori"].ToString(),
                                HaberKaynak = reader["HaberKaynak"].ToString(),
                                HaberLink = reader["HaberLink"].ToString(),
                                //HaberNot = reader["HaberNot"].ToString(),
                                HaberOkunma = Convert.ToInt32(reader["HaberOkunma"].ToString()),
                                //HaberOzet = reader["HaberOzet"].ToString(),
                                //HaberPuan = Convert.ToInt32(reader["HaberPuan"].ToString()),
                                HaberMetaBaslik = reader["HaberMetaBaslik"].ToString(),
                                HaberMetaKisaBaslik = reader["HaberMetaKisaBaslik"].ToString(),
                                //HaberMetaGorsel = reader["HaberMetaGorsel"].ToString(),
                                HaberOkunmaSuresi = Convert.ToInt32(reader["HaberOkunmaSuresi"].ToString())
                                //HaberKaynakAdi = reader["HaberKaynakAdi"].ToString(),
                                //HaberKategoriAdi = reader["HaberKategoriAdi"].ToString(),
                                //HaberOlusturulma = Convert.ToDateTime(reader["HaberOlusturulma"].ToString())
                            });
                        }

                        reader.Close();
                        cn.Close();
                    }
                }
                else if (!string.IsNullOrEmpty(kaynak) && kaynak != "tumu")
                {
                    using (SqlConnection cn = new SqlConnection(connectionString))
                    using (SqlCommand cmd = new SqlCommand("SELECT TOP (@_miktar) HaberBaslik, HaberBaslikSeo, HaberEklenme, HaberGorsel, HaberKategori, HaberKaynak, HaberLink, HaberOkunma, HaberMetaBaslik, HaberMetaKisaBaslik, HaberOkunmaSuresi FROM Haberler WHERE HaberKaynak = @_kaynak ORDER BY HaberEklenme DESC", cn))
                    {
                        cmd.Parameters.AddWithValue("@_miktar", (miktar == 0 ? 1000 : miktar));
                        cmd.Parameters.AddWithValue("@_kaynak", kaynak);

                        cn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            list.Add(new Haber
                            {
                                HaberBaslik = reader["HaberBaslik"].ToString(),
                                HaberBaslikSeo = reader["HaberBaslikSeo"].ToString(),
                                //HaberDurum = Convert.ToBoolean(reader["HaberDurum"].ToString()),
                                HaberEklenme = Convert.ToDateTime(reader["HaberEklenme"].ToString()),
                                //HaberEtiket = reader["HaberEtiket"].ToString(),
                                HaberGorsel = reader["HaberGorsel"].ToString(),
                                //HaberIcerik = reader["HaberIcerik"].ToString(),
                                //HaberID = Convert.ToInt32(reader["HaberID"].ToString()),
                                HaberKategori = reader["HaberKategori"].ToString(),
                                HaberKaynak = reader["HaberKaynak"].ToString(),
                                HaberLink = reader["HaberLink"].ToString(),
                                //HaberNot = reader["HaberNot"].ToString(),
                                HaberOkunma = Convert.ToInt32(reader["HaberOkunma"].ToString()),
                                //HaberOzet = reader["HaberOzet"].ToString(),
                                //HaberPuan = Convert.ToInt32(reader["HaberPuan"].ToString()),
                                HaberMetaBaslik = reader["HaberMetaBaslik"].ToString(),
                                HaberMetaKisaBaslik = reader["HaberMetaKisaBaslik"].ToString(),
                                //HaberMetaGorsel = reader["HaberMetaGorsel"].ToString(),
                                HaberOkunmaSuresi = Convert.ToInt32(reader["HaberOkunmaSuresi"].ToString())
                                //HaberKaynakAdi = reader["HaberKaynakAdi"].ToString(),
                                //HaberKategoriAdi = reader["HaberKategoriAdi"].ToString(),
                                //HaberOlusturulma = Convert.ToDateTime(reader["HaberOlusturulma"].ToString())
                            });
                        }

                        reader.Close();
                        cn.Close();
                    }
                }
                else
                {
                    using (SqlConnection cn = new SqlConnection(connectionString))
                    using (SqlCommand cmd = new SqlCommand("SELECT TOP (@_miktar) HaberBaslik, HaberBaslikSeo, HaberEklenme, HaberGorsel, HaberKategori, HaberKaynak, HaberLink, HaberOkunma, HaberMetaBaslik, HaberMetaKisaBaslik, HaberOkunmaSuresi FROM Haberler ORDER BY HaberEklenme DESC", cn))
                    {
                        cmd.Parameters.AddWithValue("@_miktar", (miktar == 0 ? 1000 : miktar));

                        cn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            list.Add(new Haber
                            {
                                HaberBaslik = reader["HaberBaslik"].ToString(),
                                HaberBaslikSeo = reader["HaberBaslikSeo"].ToString(),
                                //HaberDurum = Convert.ToBoolean(reader["HaberDurum"].ToString()),
                                HaberEklenme = Convert.ToDateTime(reader["HaberEklenme"].ToString()),
                                //HaberEtiket = reader["HaberEtiket"].ToString(),
                                HaberGorsel = reader["HaberGorsel"].ToString(),
                                //HaberIcerik = reader["HaberIcerik"].ToString(),
                                //HaberID = Convert.ToInt32(reader["HaberID"].ToString()),
                                HaberKategori = reader["HaberKategori"].ToString(),
                                HaberKaynak = reader["HaberKaynak"].ToString(),
                                HaberLink = reader["HaberLink"].ToString(),
                                //HaberNot = reader["HaberNot"].ToString(),
                                HaberOkunma = Convert.ToInt32(reader["HaberOkunma"].ToString()),
                                //HaberOzet = reader["HaberOzet"].ToString(),
                                //HaberPuan = Convert.ToInt32(reader["HaberPuan"].ToString()),
                                HaberMetaBaslik = reader["HaberMetaBaslik"].ToString(),
                                HaberMetaKisaBaslik = reader["HaberMetaKisaBaslik"].ToString(),
                                //HaberMetaGorsel = reader["HaberMetaGorsel"].ToString(),
                                HaberOkunmaSuresi = Convert.ToInt32(reader["HaberOkunmaSuresi"].ToString())
                                //HaberKaynakAdi = reader["HaberKaynakAdi"].ToString(),
                                //HaberKategoriAdi = reader["HaberKategoriAdi"].ToString(),
                                //HaberOlusturulma = Convert.ToDateTime(reader["HaberOlusturulma"].ToString())
                            });
                        }

                        reader.Close();
                        cn.Close();
                    }
                }
            }
            catch (Exception ex) { var x = ex; var xx = ex.Message; }

            return list;
        }

        public List<Haber> addList(string kategori, string kaynak, int miktar)
        {
            List<Haber> list = new List<Haber>();

            try
            {
                if (kategori.ToLower() == "undefined" || kategori.ToLower() == "default" || kategori.ToLower() == "index")
                {
                    kategori = "";
                }
                if (kaynak.ToLower() == "undefined" || kaynak.ToLower() == "default" || kaynak.ToLower() == "index")
                {
                    kaynak = "";
                }

                if (!string.IsNullOrEmpty(kategori) && kaynak != "tumu" && !string.IsNullOrEmpty(kaynak))
                {
                    using (SqlConnection cn = new SqlConnection(connectionString))
                    using (SqlCommand cmd = new SqlCommand("SELECT HaberBaslik, HaberBaslikSeo, HaberEklenme, HaberGorsel, HaberKategori, HaberKaynak, HaberLink, HaberOkunma, HaberMetaBaslik, HaberMetaKisaBaslik, HaberOkunmaSuresi FROM Haberler WHERE HaberKategori = @_kategori AND HaberKaynak = @_kaynak ORDER BY HaberEklenme DESC OFFSET @_miktar ROWS FETCH NEXT 30 ROWS ONLY", cn))
                    {
                        cmd.Parameters.AddWithValue("@_miktar", miktar);
                        cmd.Parameters.AddWithValue("@_kategori", kategori);
                        cmd.Parameters.AddWithValue("@_kaynak", kaynak);

                        cn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            list.Add(new Haber
                            {
                                HaberBaslik = reader["HaberBaslik"].ToString(),
                                HaberBaslikSeo = reader["HaberBaslikSeo"].ToString(),
                                //HaberDurum = Convert.ToBoolean(reader["HaberDurum"].ToString()),
                                HaberEklenme = Convert.ToDateTime(reader["HaberEklenme"].ToString()),
                                //HaberEtiket = reader["HaberEtiket"].ToString(),
                                HaberGorsel = reader["HaberGorsel"].ToString(),
                                //HaberIcerik = reader["HaberIcerik"].ToString(),
                                //HaberID = Convert.ToInt32(reader["HaberID"].ToString()),
                                HaberKategori = reader["HaberKategori"].ToString(),
                                HaberKaynak = reader["HaberKaynak"].ToString(),
                                HaberLink = reader["HaberLink"].ToString(),
                                //HaberNot = reader["HaberNot"].ToString(),
                                HaberOkunma = Convert.ToInt32(reader["HaberOkunma"].ToString()),
                                //HaberOzet = reader["HaberOzet"].ToString(),
                                //HaberPuan = Convert.ToInt32(reader["HaberPuan"].ToString()),
                                HaberMetaBaslik = reader["HaberMetaBaslik"].ToString(),
                                HaberMetaKisaBaslik = reader["HaberMetaKisaBaslik"].ToString(),
                                //HaberMetaGorsel = reader["HaberMetaGorsel"].ToString(),
                                HaberOkunmaSuresi = Convert.ToInt32(reader["HaberOkunmaSuresi"].ToString())
                                //HaberKaynakAdi = reader["HaberKaynakAdi"].ToString(),
                                //HaberKategoriAdi = reader["HaberKategoriAdi"].ToString(),
                                //HaberOlusturulma = Convert.ToDateTime(reader["HaberOlusturulma"].ToString())
                            });
                        }

                        reader.Close();
                        cn.Close();
                    }
                }
                else if (!string.IsNullOrEmpty(kategori))
                {
                    using (SqlConnection cn = new SqlConnection(connectionString))
                    using (SqlCommand cmd = new SqlCommand("SELECT HaberBaslik, HaberBaslikSeo, HaberEklenme, HaberGorsel, HaberKategori, HaberKaynak, HaberLink, HaberOkunma, HaberMetaBaslik, HaberMetaKisaBaslik, HaberOkunmaSuresi FROM Haberler WHERE HaberKategori = @_kategori ORDER BY HaberEklenme DESC OFFSET @_miktar ROWS FETCH NEXT 30 ROWS ONLY", cn))
                    {
                        cmd.Parameters.AddWithValue("@_miktar", miktar);
                        cmd.Parameters.AddWithValue("@_kategori", kategori);

                        cn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            list.Add(new Haber
                            {
                                HaberBaslik = reader["HaberBaslik"].ToString(),
                                HaberBaslikSeo = reader["HaberBaslikSeo"].ToString(),
                                //HaberDurum = Convert.ToBoolean(reader["HaberDurum"].ToString()),
                                HaberEklenme = Convert.ToDateTime(reader["HaberEklenme"].ToString()),
                                //HaberEtiket = reader["HaberEtiket"].ToString(),
                                HaberGorsel = reader["HaberGorsel"].ToString(),
                                //HaberIcerik = reader["HaberIcerik"].ToString(),
                                //HaberID = Convert.ToInt32(reader["HaberID"].ToString()),
                                HaberKategori = reader["HaberKategori"].ToString(),
                                HaberKaynak = reader["HaberKaynak"].ToString(),
                                HaberLink = reader["HaberLink"].ToString(),
                                //HaberNot = reader["HaberNot"].ToString(),
                                HaberOkunma = Convert.ToInt32(reader["HaberOkunma"].ToString()),
                                //HaberOzet = reader["HaberOzet"].ToString(),
                                //HaberPuan = Convert.ToInt32(reader["HaberPuan"].ToString()),
                                HaberMetaBaslik = reader["HaberMetaBaslik"].ToString(),
                                HaberMetaKisaBaslik = reader["HaberMetaKisaBaslik"].ToString(),
                                //HaberMetaGorsel = reader["HaberMetaGorsel"].ToString(),
                                HaberOkunmaSuresi = Convert.ToInt32(reader["HaberOkunmaSuresi"].ToString())
                                //HaberKaynakAdi = reader["HaberKaynakAdi"].ToString(),
                                //HaberKategoriAdi = reader["HaberKategoriAdi"].ToString(),
                                //HaberOlusturulma = Convert.ToDateTime(reader["HaberOlusturulma"].ToString())
                            });
                        }

                        reader.Close();
                        cn.Close();
                    }
                }
                else if (!string.IsNullOrEmpty(kaynak) && kaynak != "tumu")
                {
                    using (SqlConnection cn = new SqlConnection(connectionString))
                    using (SqlCommand cmd = new SqlCommand("SELECT HaberBaslik, HaberBaslikSeo, HaberEklenme, HaberGorsel, HaberKategori, HaberKaynak, HaberLink, HaberOkunma, HaberMetaBaslik, HaberMetaKisaBaslik, HaberOkunmaSuresi FROM Haberler WHERE HaberKaynak = @_kaynak ORDER BY HaberEklenme DESC OFFSET @_miktar ROWS FETCH NEXT 30 ROWS ONLY", cn))
                    {
                        cmd.Parameters.AddWithValue("@_miktar", (miktar == 0 ? 1000 : miktar));
                        cmd.Parameters.AddWithValue("@_kaynak", kaynak);

                        cn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            list.Add(new Haber
                            {
                                HaberBaslik = reader["HaberBaslik"].ToString(),
                                HaberBaslikSeo = reader["HaberBaslikSeo"].ToString(),
                                //HaberDurum = Convert.ToBoolean(reader["HaberDurum"].ToString()),
                                HaberEklenme = Convert.ToDateTime(reader["HaberEklenme"].ToString()),
                                //HaberEtiket = reader["HaberEtiket"].ToString(),
                                HaberGorsel = reader["HaberGorsel"].ToString(),
                                //HaberIcerik = reader["HaberIcerik"].ToString(),
                                //HaberID = Convert.ToInt32(reader["HaberID"].ToString()),
                                HaberKategori = reader["HaberKategori"].ToString(),
                                HaberKaynak = reader["HaberKaynak"].ToString(),
                                HaberLink = reader["HaberLink"].ToString(),
                                //HaberNot = reader["HaberNot"].ToString(),
                                HaberOkunma = Convert.ToInt32(reader["HaberOkunma"].ToString()),
                                //HaberOzet = reader["HaberOzet"].ToString(),
                                //HaberPuan = Convert.ToInt32(reader["HaberPuan"].ToString()),
                                HaberMetaBaslik = reader["HaberMetaBaslik"].ToString(),
                                HaberMetaKisaBaslik = reader["HaberMetaKisaBaslik"].ToString(),
                                //HaberMetaGorsel = reader["HaberMetaGorsel"].ToString(),
                                HaberOkunmaSuresi = Convert.ToInt32(reader["HaberOkunmaSuresi"].ToString())
                                //HaberKaynakAdi = reader["HaberKaynakAdi"].ToString(),
                                //HaberKategoriAdi = reader["HaberKategoriAdi"].ToString(),
                                //HaberOlusturulma = Convert.ToDateTime(reader["HaberOlusturulma"].ToString())
                            });
                        }

                        reader.Close();
                        cn.Close();
                    }
                }
                else
                {
                    using (SqlConnection cn = new SqlConnection(connectionString))
                    using (SqlCommand cmd = new SqlCommand("SELECT HaberBaslik, HaberBaslikSeo, HaberEklenme, HaberGorsel, HaberKategori, HaberKaynak, HaberLink, HaberOkunma, HaberMetaBaslik, HaberMetaKisaBaslik, HaberOkunmaSuresi FROM Haberler ORDER BY HaberEklenme DESC OFFSET @_miktar ROWS FETCH NEXT 30 ROWS ONLY", cn))
                    {
                        cmd.Parameters.AddWithValue("@_miktar", (miktar == 0 ? 1000 : miktar));

                        cn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            list.Add(new Haber
                            {
                                HaberBaslik = reader["HaberBaslik"].ToString(),
                                HaberBaslikSeo = reader["HaberBaslikSeo"].ToString(),
                                //HaberDurum = Convert.ToBoolean(reader["HaberDurum"].ToString()),
                                HaberEklenme = Convert.ToDateTime(reader["HaberEklenme"].ToString()),
                                //HaberEtiket = reader["HaberEtiket"].ToString(),
                                HaberGorsel = reader["HaberGorsel"].ToString(),
                                //HaberIcerik = reader["HaberIcerik"].ToString(),
                                //HaberID = Convert.ToInt32(reader["HaberID"].ToString()),
                                HaberKategori = reader["HaberKategori"].ToString(),
                                HaberKaynak = reader["HaberKaynak"].ToString(),
                                HaberLink = reader["HaberLink"].ToString(),
                                //HaberNot = reader["HaberNot"].ToString(),
                                HaberOkunma = Convert.ToInt32(reader["HaberOkunma"].ToString()),
                                //HaberOzet = reader["HaberOzet"].ToString(),
                                //HaberPuan = Convert.ToInt32(reader["HaberPuan"].ToString()),
                                HaberMetaBaslik = reader["HaberMetaBaslik"].ToString(),
                                HaberMetaKisaBaslik = reader["HaberMetaKisaBaslik"].ToString(),
                                //HaberMetaGorsel = reader["HaberMetaGorsel"].ToString(),
                                HaberOkunmaSuresi = Convert.ToInt32(reader["HaberOkunmaSuresi"].ToString())
                                //HaberKaynakAdi = reader["HaberKaynakAdi"].ToString(),
                                //HaberKategoriAdi = reader["HaberKategoriAdi"].ToString(),
                                //HaberOlusturulma = Convert.ToDateTime(reader["HaberOlusturulma"].ToString())
                            });
                        }

                        reader.Close();
                        cn.Close();
                    }
                }
            }
            catch (Exception ex) { var x = ex; var xx = ex.Message; }

            return list;
        }

        public List<MenuItem> menu()
        {
            List<string> mik = new List<string>();
            List<MenuItem> mi = new List<MenuItem>();

            using (SqlConnection cn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand("SELECT DISTINCT HaberKaynak, HaberKaynakAdi FROM Haberler ORDER BY HaberKaynak ASC", cn))
            {
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    mik.Add(reader["HaberKaynak"].ToString() + "," + reader["HaberKaynakAdi"].ToString());
                }

                reader.Close();
                cn.Close();
            }

            foreach (var item in mik)
            {
                using (SqlConnection cn = new SqlConnection(connectionString))
                using (SqlCommand cmd = new SqlCommand("SELECT DISTINCT HaberKategori, HaberKategoriAdi FROM Haberler WHERE HaberKaynak = @_kaynak ORDER BY HaberKategori ASC", cn))
                {
                    cmd.Parameters.AddWithValue("@_kaynak", item.ToString().Split(',')[0]);

                    cn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    List<Kategori> k = new List<Kategori>();

                    while (reader.Read())
                    {
                        k.Add(new Kategori
                        {
                            KategoriURL = reader["HaberKategori"].ToString(),
                            KategoriAdi = reader["HaberKategoriAdi"].ToString()
                        });
                    }

                    mi.Add(new MenuItem
                    {
                        MenuKaynak = item.ToString().Split(',')[0],
                        MenuKaynakAdi = item.ToString().Split(',')[1],
                        MenuKategori = k
                    });

                    reader.Close();
                    cn.Close();
                }
            }

            return mi;
        }

        public List<Haber> listForTimeline(string kategori, string kaynak)
        {
            List<Haber> list = new List<Haber>();

            try
            {
                if (kategori.ToLower() == "undefined" || kategori.ToLower() == "default" || kategori.ToLower() == "index")
                {
                    kategori = "";
                }
                if (kaynak.ToLower() == "undefined" || kaynak.ToLower() == "default" || kaynak.ToLower() == "index")
                {
                    kaynak = "";
                }

                if (!string.IsNullOrEmpty(kategori) && !string.IsNullOrEmpty(kaynak) && kaynak != "tumu")
                {
                    using (SqlConnection cn = new SqlConnection(connectionString))
                    using (SqlCommand cmd = new SqlCommand("SELECT HaberBaslik, HaberBaslikSeo, HaberEklenme, HaberGorsel, HaberKategori, HaberKaynak, HaberLink, HaberOkunma, HaberMetaBaslik, HaberMetaKisaBaslik, HaberOkunmaSuresi FROM Haberler WHERE HaberKategori = @_kategori AND HaberKaynak = @_kaynak ORDER BY HaberEklenme DESC", cn))
                    {
                        cmd.Parameters.AddWithValue("@_kategori", kategori);
                        cmd.Parameters.AddWithValue("@_kaynak", kaynak);

                        cn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            list.Add(new Haber
                            {
                                HaberBaslik = reader["HaberBaslik"].ToString(),
                                HaberBaslikSeo = reader["HaberBaslikSeo"].ToString(),
                                //HaberDurum = Convert.ToBoolean(reader["HaberDurum"].ToString()),
                                HaberEklenme = Convert.ToDateTime(reader["HaberEklenme"].ToString()),
                                //HaberEtiket = reader["HaberEtiket"].ToString(),
                                HaberGorsel = reader["HaberGorsel"].ToString(),
                                //HaberIcerik = reader["HaberIcerik"].ToString(),
                                //HaberID = Convert.ToInt32(reader["HaberID"].ToString()),
                                HaberKategori = reader["HaberKategori"].ToString(),
                                HaberKaynak = reader["HaberKaynak"].ToString(),
                                HaberLink = reader["HaberLink"].ToString(),
                                //HaberNot = reader["HaberNot"].ToString(),
                                HaberOkunma = Convert.ToInt32(reader["HaberOkunma"].ToString()),
                                //HaberOzet = reader["HaberOzet"].ToString(),
                                //HaberPuan = Convert.ToInt32(reader["HaberPuan"].ToString()),
                                HaberMetaBaslik = reader["HaberMetaBaslik"].ToString(),
                                HaberMetaKisaBaslik = reader["HaberMetaKisaBaslik"].ToString(),
                                //HaberMetaGorsel = reader["HaberMetaGorsel"].ToString(),
                                HaberOkunmaSuresi = Convert.ToInt32(reader["HaberOkunmaSuresi"].ToString())
                                //HaberKaynakAdi = reader["HaberKaynakAdi"].ToString(),
                                //HaberKategoriAdi = reader["HaberKategoriAdi"].ToString(),
                                //HaberOlusturulma = Convert.ToDateTime(reader["HaberOlusturulma"].ToString())
                            });
                        }

                        reader.Close();
                        cn.Close();
                    }
                }
                else if (!string.IsNullOrEmpty(kaynak) && kaynak != "tumu")
                {
                    using (SqlConnection cn = new SqlConnection(connectionString))
                    using (SqlCommand cmd = new SqlCommand("SELECT HaberBaslik, HaberBaslikSeo, HaberEklenme, HaberGorsel, HaberKategori, HaberKaynak, HaberLink, HaberOkunma, HaberMetaBaslik, HaberMetaKisaBaslik, HaberOkunmaSuresi FROM Haberler WHERE HaberKaynak = @_kaynak ORDER BY HaberEklenme DESC", cn))
                    {
                        cmd.Parameters.AddWithValue("@_kaynak", kaynak);

                        cn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            list.Add(new Haber
                            {
                                HaberBaslik = reader["HaberBaslik"].ToString(),
                                HaberBaslikSeo = reader["HaberBaslikSeo"].ToString(),
                                //HaberDurum = Convert.ToBoolean(reader["HaberDurum"].ToString()),
                                HaberEklenme = Convert.ToDateTime(reader["HaberEklenme"].ToString()),
                                //HaberEtiket = reader["HaberEtiket"].ToString(),
                                HaberGorsel = reader["HaberGorsel"].ToString(),
                                //HaberIcerik = reader["HaberIcerik"].ToString(),
                                //HaberID = Convert.ToInt32(reader["HaberID"].ToString()),
                                HaberKategori = reader["HaberKategori"].ToString(),
                                HaberKaynak = reader["HaberKaynak"].ToString(),
                                HaberLink = reader["HaberLink"].ToString(),
                                //HaberNot = reader["HaberNot"].ToString(),
                                HaberOkunma = Convert.ToInt32(reader["HaberOkunma"].ToString()),
                                //HaberOzet = reader["HaberOzet"].ToString(),
                                //HaberPuan = Convert.ToInt32(reader["HaberPuan"].ToString()),
                                HaberMetaBaslik = reader["HaberMetaBaslik"].ToString(),
                                HaberMetaKisaBaslik = reader["HaberMetaKisaBaslik"].ToString(),
                                //HaberMetaGorsel = reader["HaberMetaGorsel"].ToString(),
                                HaberOkunmaSuresi = Convert.ToInt32(reader["HaberOkunmaSuresi"].ToString())
                                //HaberKaynakAdi = reader["HaberKaynakAdi"].ToString(),
                                //HaberKategoriAdi = reader["HaberKategoriAdi"].ToString(),
                                //HaberOlusturulma = Convert.ToDateTime(reader["HaberOlusturulma"].ToString())
                            });
                        }

                        reader.Close();
                        cn.Close();
                    }
                }
                else if (!string.IsNullOrEmpty(kategori))
                {
                    using (SqlConnection cn = new SqlConnection(connectionString))
                    using (SqlCommand cmd = new SqlCommand("SELECT HaberBaslik, HaberBaslikSeo, HaberEklenme, HaberGorsel, HaberKategori, HaberKaynak, HaberLink, HaberOkunma, HaberMetaBaslik, HaberMetaKisaBaslik, HaberOkunmaSuresi FROM Haberler WHERE HaberKategori = @_kategori ORDER BY HaberEklenme DESC", cn))
                    {
                        cmd.Parameters.AddWithValue("@_kategori", kategori);

                        cn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            list.Add(new Haber
                            {
                                HaberBaslik = reader["HaberBaslik"].ToString(),
                                HaberBaslikSeo = reader["HaberBaslikSeo"].ToString(),
                                //HaberDurum = Convert.ToBoolean(reader["HaberDurum"].ToString()),
                                HaberEklenme = Convert.ToDateTime(reader["HaberEklenme"].ToString()),
                                //HaberEtiket = reader["HaberEtiket"].ToString(),
                                HaberGorsel = reader["HaberGorsel"].ToString(),
                                //HaberIcerik = reader["HaberIcerik"].ToString(),
                                //HaberID = Convert.ToInt32(reader["HaberID"].ToString()),
                                HaberKategori = reader["HaberKategori"].ToString(),
                                HaberKaynak = reader["HaberKaynak"].ToString(),
                                HaberLink = reader["HaberLink"].ToString(),
                                //HaberNot = reader["HaberNot"].ToString(),
                                HaberOkunma = Convert.ToInt32(reader["HaberOkunma"].ToString()),
                                //HaberOzet = reader["HaberOzet"].ToString(),
                                //HaberPuan = Convert.ToInt32(reader["HaberPuan"].ToString()),
                                HaberMetaBaslik = reader["HaberMetaBaslik"].ToString(),
                                HaberMetaKisaBaslik = reader["HaberMetaKisaBaslik"].ToString(),
                                //HaberMetaGorsel = reader["HaberMetaGorsel"].ToString(),
                                HaberOkunmaSuresi = Convert.ToInt32(reader["HaberOkunmaSuresi"].ToString())
                                //HaberKaynakAdi = reader["HaberKaynakAdi"].ToString(),
                                //HaberKategoriAdi = reader["HaberKategoriAdi"].ToString(),
                                //HaberOlusturulma = Convert.ToDateTime(reader["HaberOlusturulma"].ToString())
                            });
                        }

                        reader.Close();
                        cn.Close();
                    }
                }
                else { }
            }
            catch { }

            return list;
        }

        public List<string> listForBot(string kaynak)
        {
            List<string> list = new List<string>();

            try
            {
                using (SqlConnection cn = new SqlConnection(connectionString))
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Haberler WHERE HaberKaynak = @_kaynak ORDER BY HaberOkunma DESC", cn))
                {
                    cmd.Parameters.AddWithValue("@_kaynak", kaynak);

                    cn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        list.Add(reader["HaberKaynak"].ToString());
                    }

                    reader.Close();
                    cn.Close();
                }
            }
            catch { }

            return list;
        }

        public Haber detail(string kaynak, string kategori, string seo)
        {
            Haber list = new Haber();

            try
            {
                int haberID = 0;

                using (SqlConnection cn = new SqlConnection(connectionString))
                using (SqlCommand cmd = new SqlCommand("SELECT TOP 1 * FROM Haberler WHERE HaberBaslikSeo = @_seo AND HaberKaynak = @_kaynak AND HaberKategori = @_kategori ORDER BY HaberEklenme DESC", cn))
                {
                    cmd.Parameters.AddWithValue("@_seo", seo);
                    cmd.Parameters.AddWithValue("@_kaynak", kaynak);
                    cmd.Parameters.AddWithValue("@_kategori", kategori);

                    cn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        haberID = Convert.ToInt32(reader["HaberID"].ToString());
                        list.HaberBaslik = reader["HaberBaslik"].ToString();
                        list.HaberBaslikSeo = reader["HaberBaslikSeo"].ToString();
                        list.HaberDurum = Convert.ToBoolean(reader["HaberDurum"].ToString());
                        list.HaberEklenme = Convert.ToDateTime(reader["HaberEklenme"].ToString());
                        list.HaberEtiket = reader["HaberEtiket"].ToString();
                        list.HaberGorsel = reader["HaberGorsel"].ToString();
                        list.HaberIcerik = reader["HaberIcerik"].ToString();
                        list.HaberID = Convert.ToInt32(reader["HaberID"].ToString());
                        list.HaberKategori = reader["HaberKategori"].ToString();
                        list.HaberKaynak = reader["HaberKaynak"].ToString();
                        list.HaberLink = reader["HaberLink"].ToString();
                        list.HaberNot = reader["HaberNot"].ToString();
                        list.HaberOkunma = Convert.ToInt32(reader["HaberOkunma"].ToString());
                        list.HaberOzet = reader["HaberOzet"].ToString();
                        list.HaberPuan = Convert.ToInt32(reader["HaberPuan"].ToString());
                        list.HaberMetaBaslik = reader["HaberMetaBaslik"].ToString();
                        list.HaberMetaKisaBaslik = reader["HaberMetaKisaBaslik"].ToString();
                        list.HaberMetaGorsel = reader["HaberMetaGorsel"].ToString();
                        list.HaberOkunmaSuresi = Convert.ToInt32(reader["HaberOkunmaSuresi"].ToString());
                        list.HaberKaynakAdi = reader["HaberKaynakAdi"].ToString();
                        list.HaberKategoriAdi = reader["HaberKategoriAdi"].ToString();
                        list.HaberOlusturulma = Convert.ToDateTime(reader["HaberOlusturulma"].ToString());
                    }

                    increase(haberID);

                    reader.Close();
                    cn.Close();
                }
            }
            catch { }

            return list;
        }

        public void increase(int id)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connectionString))
                using (SqlCommand cmd = new SqlCommand(updateCountQuery, cn))
                {
                    cmd.Parameters.AddWithValue("@id", id);

                    cn.Open();
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch { }
        }
    }
}