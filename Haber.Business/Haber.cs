﻿using System;

namespace Haber.Business
{
    public class Haber
    {
        public int HaberID { get; set; }
        public string HaberKaynak { get; set; }
        public string HaberKategori { get; set; }
        public string HaberLink { get; set; }
        public string HaberBaslik { get; set; }
        public string HaberBaslikSeo { get; set; }
        public string HaberGorsel { get; set; }
        public string HaberIcerik { get; set; }
        public string HaberEtiket { get; set; }
        public string HaberOzet { get; set; }
        public int HaberPuan { get; set; }
        public string HaberNot { get; set; }
        public int HaberOkunma { get; set; }
        public DateTime HaberEklenme { get; set; }
        public bool HaberDurum { get; set; }
        public string HaberMetaBaslik { get; set; }
        public string HaberMetaKisaBaslik { get; set; }
        public string HaberMetaGorsel { get; set; }
        public int HaberOkunmaSuresi { get; set; }
        public string HaberKaynakAdi { get; set; }
        public string HaberKategoriAdi { get; set; }
        public DateTime HaberOlusturulma { get; set; }
    }
}