﻿using System;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Haber.Console
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            GetMenu();
        }

        private static void GetMenu()
        {
            System.Console.WriteLine("GAZETE SECINIZ : ");
            System.Console.WriteLine("SABAH - 1");
            System.Console.WriteLine("MILLIYET - 2");
            System.Console.WriteLine("HURRIYET - 3");
            System.Console.WriteLine("SOZCU - 4");
            System.Console.WriteLine("YENIAKIT - 5");
            System.Console.WriteLine("FANATIK - 6");
            System.Console.WriteLine("FOTOMAC - 7");
            System.Console.WriteLine("POSTA - 8");
            System.Console.WriteLine("HEPSINI CALISTIR - 98");
            System.Console.WriteLine("EKRANI TEMIZLE - 99");
            System.Console.WriteLine("ESKI HABERLERI UCUR - 988111988");
            System.Console.WriteLine("HABERLER TABLOSUNU TEMIZLE - 999111999");
            System.Console.WriteLine("CIKIS YAP - 100");
            GetProcess(Convert.ToInt32(System.Console.ReadLine()));
        }

        private static void GetProcess(int rl)
        {
            Business.BotService BS = new Business.BotService();
            Business.HaberService HS = new Business.HaberService();

            switch (rl)
            {
                case (1): //SABAH
                    int sabahSay = BS.SabahBot();
                    System.Console.WriteLine("ISLEM SONUCU : SABAH SITESINDEN " + sabahSay.ToString() + " HABER EKLENDI.");
                    GetMenu();
                    break;

                case (2): //MILLIYET
                    int milliyetSay = BS.MilliyetBot();
                    System.Console.WriteLine("ISLEM SONUCU : MILLIYET SITESINDEN " + milliyetSay.ToString() + " HABER EKLENDI.");
                    GetMenu();
                    break;

                //case (3): //HURRIYET
                //    int hurriyetSay = BS.HurriyetBot();
                //    System.Console.WriteLine("ISLEM SONUCU : HURRIYET SITESINDEN " + hurriyetSay.ToString() + " HABER EKLENDI.");
                //    GetMenu();
                //    break;

                //case (4): //SOZCU
                //    int sozcuSay = BS.SozcuBot();
                //    System.Console.WriteLine("ISLEM SONUCU : SOZCU SITESINDEN " + sozcuSay.ToString() + " HABER EKLENDI.");
                //    GetMenu();
                //    break;

                case (5): //YENIAKIT
                    int yeniAkitSay = BS.YeniAkitBot();
                    System.Console.WriteLine("ISLEM SONUCU : YENIAKIT SITESINDEN " + yeniAkitSay.ToString() + " HABER EKLENDI.");
                    GetMenu();
                    break;

                case (6): //FANATIK
                    int fanatikSay = BS.FanatikBot();
                    System.Console.WriteLine("ISLEM SONUCU : FANATIK SITESINDEN " + fanatikSay.ToString() + " HABER EKLENDI.");
                    GetMenu();
                    break;

                case (7): //FOTOMAC
                    int fotomacSay = BS.FotomacBot();
                    System.Console.WriteLine("ISLEM SONUCU : FOTOMAC SITESINDEN " + fotomacSay.ToString() + " HABER EKLENDI.");
                    GetMenu();
                    break;

                case (8): //POSTA
                    int postaSay = BS.PostaBot();
                    System.Console.WriteLine("ISLEM POSTA : POSTA SITESINDEN " + postaSay.ToString() + " HABER EKLENDI.");
                    GetMenu();
                    break;

                case (98): //ASENKRON

                    #region ASENKRON DÜŞÜK PERFORMANS

                    //Thread th1 = new Thread(() => BS.SabahBot());
                    //Thread th2 = new Thread(() => BS.MilliyetBot());
                    //Thread th3 = new Thread(() => BS.HurriyetBot());
                    ////Thread th4 = new Thread(() => BS.SozcuBot());
                    ////Thread th5 = new Thread(() => BS.YeniAkitBot());
                    ////Thread th6 = new Thread(() => BS.FanatikBot());
                    ////Thread th7 = new Thread(() => BS.FotomacBot());
                    ////Thread th8 = new Thread(() => BS.PostaBot());
                    //th1.Start();
                    //th2.Start();
                    //th3.Start();
                    ////th4.Start();
                    ////th5.Start();
                    ////th6.Start();
                    ////th7.Start();
                    ////th8.Start();

                    ////TÜM İŞLEMLERİN TAMAMLANDIĞINI ANLAMAK İÇİN JOİN GEREKİYO / START MUTLAKA JOİNDEN ÖNCE OLMALI
                    ////th1.Join();
                    ////th2.Join();
                    ////th3.Join();
                    ////th4.Join();
                    ////th5.Join();
                    ////th6.Join();
                    ////th7.Join();
                    ////th8.Join();

                    ////System.Console.WriteLine("ISLEM SONUCU : BUTUN SITELER TARANDI");

                    #endregion

                    #region ASENKRON YÜKSEK PERFORMANS

                    Task task1 = Task.Factory.StartNew(() => BS.SabahBot());
                    Task task2 = Task.Factory.StartNew(() => BS.MilliyetBot());
                    //Task task3 = Task.Factory.StartNew(() => BS.HurriyetBot());
                    //Task task4 = Task.Factory.StartNew(() => BS.SozcuBot());
                    Task task5 = Task.Factory.StartNew(() => BS.YeniAkitBot());
                    Task task6 = Task.Factory.StartNew(() => BS.FanatikBot());
                    Task task7 = Task.Factory.StartNew(() => BS.FotomacBot());
                    Task task8 = Task.Factory.StartNew(() => BS.PostaBot());

                    //Task.WaitAll(task1, task2, task3, task4, task5, task6, task7, task8);
                    Task.WaitAll(task1, task2, task5, task6, task7, task8);
                    System.Console.WriteLine("ISLEM SONUCU : BUTUN SITELER TARANDI");

                    #endregion

                    GetMenu();
                    break;

                case (988111988): //ESKI HABERLERI UCUR
                    string eskiResult = HS.clear();
                    System.Console.WriteLine("ISLEM SONUCU : " + eskiResult);
                    GetMenu();
                    break;

                case (99): //EKRANI TEMIZLE
                    System.Console.Clear();
                    GetMenu();
                    break;

                case (100):
                    Environment.Exit(0);
                    break;

                case (999111999):
                    string truncateResult = HS.truncate();
                    System.Console.WriteLine("ISLEM SONUCU : " + truncateResult);
                    GetMenu();
                    break;

                default:
                    System.Console.WriteLine("HATALI SECIM...");
                    GetMenu();
                    break;
            }
        }
    }
}