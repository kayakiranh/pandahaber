﻿using Haber.Business;
using System.ServiceProcess;
using System.Timers;

namespace Haber.WinService
{
    public partial class Service1 : ServiceBase
    {
        private static Timer aTimer;

        public Service1()
        {
            //1 gün = 86400000 ms
            aTimer = new System.Timers.Timer(86400000);
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);

            //1 günün içindeki her 10 dakikada bir ontimedevent fonksiyonunu aktif et
            aTimer.Interval = 600000;
            aTimer.Enabled = true;

            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
        }

        protected override void OnStop()
        {
        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            BotService BS = new BotService();
            HaberService HS = new HaberService();
            var s0 = HS.clear();
            var s1 = BS.SabahBot();
            var s2 = BS.MilliyetBot();
        }
    }
}