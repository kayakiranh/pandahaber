﻿namespace Haber.Web.Models
{
    public class ExtraHaber
    {
        public string HaberGorsel { get; set; }
        public string HaberBaslik { get; set; }
        public string HaberKaynak { get; set; }
        public string HaberKategori { get; set; }
        public string HaberBaslikSeo { get; set; }
        public string HaberMetaBaslik { get; set; }
        public string HaberEklenme { get; set; }
        public int HaberOkunma { get; set; }
        public int HaberOkunmaSuresi { get; set; }
        public string HaberLink { get; set; }
        public string HaberMetaKisaBaslik { get; set; }
    }
}