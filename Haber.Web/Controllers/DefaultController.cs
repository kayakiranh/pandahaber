﻿using Haber.Business;
using Haber.Web.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Haber.Web.Controllers
{
    public class DefaultController : Controller
    {
        //[OutputCache(Duration = 300)]
        public ActionResult index(string kaynak, string kategori)
        {
            var data = new List<Business.Haber>();
            HaberService HS = new HaberService();

            if (!string.IsNullOrEmpty(kategori) && !string.IsNullOrEmpty(kaynak))
            {
                data = HS.listMain(kategori, kaynak, 90);
                ViewBag.URL = "http://www.pandahaber.com/" + kaynak + "/" + kategori;
                ViewBag.Title = "Panda Haber | Panda'dan al haberi !";
                ViewBag.Description = "Panda Haber | " + kaynak + " sitesinden " + kategori + " haberleri gösteriliyor.";
            }
            else if (string.IsNullOrEmpty(kategori) && !string.IsNullOrEmpty(kaynak))
            {
                data = HS.listMain("", kaynak, 90);
                ViewBag.URL = "http://www.pandahaber.com/" + kaynak;
                ViewBag.Title = "Panda Haber | Panda'dan al haberi !";
                ViewBag.Description = "Panda Haber | " + kaynak + " sitesinden haberler gösteriliyor.";
            }
            else if (!string.IsNullOrEmpty(kategori) && string.IsNullOrEmpty(kaynak))
            {
                data = HS.listMain(kategori, "", 90);
                ViewBag.URL = "http://www.pandahaber.com/tumu/" + kategori;
                ViewBag.Title = "Panda Haber | Panda'dan al haberi !";
                ViewBag.Description = "Panda Haber | " + kategori + " haberleri gösteriliyor.";
            }
            else
            {
                data = HS.listMain("", "", 90);
                ViewBag.URL = "http://www.pandahaber.com/";
                ViewBag.Title = "Panda Haber | Panda'dan al haberi !";
                ViewBag.Description = "Panda Haber | Tüm haberler gösteriliyor.";
            }

            return View(data);
        }

        [OutputCache(Duration = 300, VaryByParam = "*")]
        public ActionResult akis(string kaynak, string kategori)
        {
            var data = new List<Business.Haber>();
            HaberService HS = new HaberService();
            data = HS.listForTimeline(kategori, kaynak);
            if (data.Count == 0 || data == null)
            {
                return RedirectToAction("index", "default");
            }

            if (!string.IsNullOrEmpty(kaynak) && !string.IsNullOrEmpty(kategori))
            {
                ViewBag.Title = "Panda Haber | Panda'dan al haberi !";
                ViewBag.Description = "Panda Haber | Haber Akışı";
                ViewBag.URL = "http://www.pandahaber.com/akis/" + kaynak + "/" + kategori;
            }
            else if (!string.IsNullOrEmpty(kaynak) && string.IsNullOrEmpty(kategori))
            {
                ViewBag.Title = "Panda Haber | Panda'dan al haberi !";
                ViewBag.Description = "Panda Haber | Haber Akışı";
                ViewBag.URL = "http://www.pandahaber.com/akis/" + kaynak;
            }
            else if (string.IsNullOrEmpty(kaynak) && !string.IsNullOrEmpty(kategori))
            {
                ViewBag.Title = "Panda Haber | Panda'dan al haberi !";
                ViewBag.Description = "Panda Haber | Haber Akışı";
                ViewBag.URL = "http://www.pandahaber.com/akis/tumu/" + kategori;
            }
            else
            {
                ViewBag.Title = "Panda Haber | Panda'dan al haberi !";
                ViewBag.Description = "Panda Haber | Haber Akışı";
                ViewBag.URL = "http://www.pandahaber.com/akis/" + kaynak + "/" + kategori;
            }

            return View(data);
        }

        [OutputCache(Duration = 3600, VaryByParam = "*")]
        public ActionResult haber(string kaynak, string kategori, string baslik)
        {
            var data = new Business.Haber();
            HaberService HS = new HaberService();
            data = HS.detail(kaynak, kategori, baslik);
            if (data == null || data.HaberID == 0)
            {
                return RedirectToAction("index", "default");
            }

            ViewBag.Title = "Panda Haber | Panda'dan al haberi !";
            ViewBag.Description = "Panda Haber | " + data.HaberBaslik;
            ViewBag.URL = "http://www.pandahaber.com/haber/" + kaynak + "/" + kategori + "/" + baslik;
            ViewBag.URL2 = "pandahaber.com/haber/" + kaynak + "/" + kategori + "/" + baslik;
            return View(data);
        }

        [HttpPost]
        public JsonResult getir(string kaynak, string kategori, int say)
        {
            List<ExtraHaber> haberler = new List<ExtraHaber>();
            HaberService HS = new HaberService();
            var data = HS.addList(kategori, kaynak, say);

            foreach (var item in data)
            {
                haberler.Add(new ExtraHaber
                {
                    HaberBaslik = item.HaberBaslik,
                    HaberBaslikSeo = item.HaberBaslikSeo,
                    HaberEklenme = item.HaberEklenme.ToString(),
                    HaberGorsel = item.HaberGorsel,
                    HaberKategori = item.HaberKategori,
                    HaberKaynak = item.HaberKaynak,
                    HaberMetaBaslik = item.HaberMetaBaslik,
                    HaberOkunma = item.HaberOkunma,
                    HaberOkunmaSuresi = item.HaberOkunmaSuresi,
                    HaberLink = item.HaberLink,
                    HaberMetaKisaBaslik = item.HaberMetaKisaBaslik
                });
            }

            return Json(haberler);
        }
    }
}