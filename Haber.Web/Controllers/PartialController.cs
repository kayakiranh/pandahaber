﻿using Haber.Business;
using System.Web.Mvc;

namespace Haber.Web.Controllers
{
    public class PartialController : Controller
    {
        public ActionResult menu()
        {
            HaberService HS = new HaberService();
            var data = HS.menu();
            return View("_menu", data);
        }

        public ActionResult nav()
        {
            HaberService HS = new HaberService();
            var data = HS.menu();
            return View("_nav", data);
        }

        [OutputCache(Duration = 300)]
        public ActionResult header()
        {
            return View("_header");
        }

        [OutputCache(Duration = 300)]
        public ActionResult footer()
        {
            HaberService HS = new HaberService();
            var data = HS.menu();
            return View("_footer");
        }
    }
}