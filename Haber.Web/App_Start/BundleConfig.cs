﻿using System.Web.Optimization;

namespace Haber.Web.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/style").Include("~/Assets/Theme/assets/css/master.css"));

            bundles.Add(new StyleBundle("~/bundles/script").Include("~/Assets/Theme/assets/libs/jquery-1.12.4.min.js",
                            "~/Assets/Theme/assets/libs/jquery-migrate-1.2.1.js",
                            "~/Assets/Theme/assets/libs/modernizr.custom.js",
                            "~/Assets/Theme/assets/libs/bootstrap/bootstrap.min.js",
                            "~/Assets/Theme/assets/js/custom.min.js",
                            "~/Assets/Theme/assets/plugins/headers/jquery.dlmenu.js",
                            "~/Assets/Theme/assets/plugins/headers/header.min.js",
                            "~/Assets/Theme/assets/plugins/scrollreveal/scrollreveal.min.js"));
        }
    }
}