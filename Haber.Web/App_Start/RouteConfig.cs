﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace Haber.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "getir",
                url: "getir",
                defaults: new { controller = "Default", action = "getir" }
            );

            routes.MapRoute(
                name: "akis",
                url: "akis/{kaynak}/{kategori}",
                defaults: new { controller = "Default", action = "akis", kaynak = UrlParameter.Optional, kategori = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "haber",
                url: "haber/{kaynak}/{kategori}/{baslik}",
                defaults: new { controller = "Default", action = "haber", kaynak = String.Empty, kategori = String.Empty, baslik = String.Empty }
            );

            routes.MapRoute(
                name: "anasayfa",
                url: "{kaynak}/{kategori}",
                defaults: new { controller = "Default", action = "index", kaynak = String.Empty, kategori = String.Empty }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Default", action = "index", id = UrlParameter.Optional }
            );
        }
    }
}