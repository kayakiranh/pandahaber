(function () {

    "use strict";

    var Core = {
        initialized: false,
        initialize: function () {
            if (this.initialized)
                return;
            this.initialized = true;
            this.build();
        },
        build: function () {

            this.fixedHeader();
            this.initToggleMenu();

      this.dropdownhover();

        },

   initToggleMenu: function() {


            $('.toggle-menu-button').each(function(i) {


                var trigger = $(this);
                var isClosed = true;

                function showMenu() {

                    $('#nav').addClass('navbar-scrolling-fixing');

                    if (trigger.hasClass("js-toggle-screen")) {

                        $('#fixedMenu').delay(0).fadeIn(300);

                    }

                    trigger.addClass('is-open');
                    isClosed = false;
                }

                function hideMenu() {
                    $('#fixedMenu').fadeOut(100);
                    $('#nav').removeClass('navbar-scrolling-fixing');
                    trigger.removeClass('is-open');
                    isClosed = true;
                }

                  $('.fullmenu-close').on('click', function(e) {
                      e.preventDefault();
                      if (isClosed === true) {
                          //hideMenu();
                      } else {
                          hideMenu();
                      }
                  });

                trigger.on('click', function(e) {
                    e.preventDefault();
                    if (isClosed === true) {
                        showMenu();
                    } else {
                        hideMenu();
                    }
                });

                 $(".fw-close").on('click', function(e) {
                    e.preventDefault();
                    if (isClosed === true) {
                        showMenu();
                    } else {
                        hideMenu();
                    }
                });
            });
        },



    dropdownhover: function(options) {
      /** Extra script for smoother navigation effect **/
      if ($(window).width() > 798) {
        $('.yamm').on('mouseenter', '.navbar-nav > .dropdown', function() {
          "use strict";
          $(this).addClass('open');
        }).on('mouseleave', '.navbar-nav > .dropdown', function() {
          "use strict";
          $(this).removeClass('open');
        });
      }
    },
        fixedHeader: function (options) {
            if ($(window).width() > 767) {
                // Fixed Header
                var topOffset = $(window).scrollTop();
                if (topOffset > 0) {
                    $('.header').addClass('navbar-scrolling');
                }
                $(window).on('scroll', function () {
                    var fromTop = $(this).scrollTop();
                    if (fromTop > 0) {
                        $('body').addClass('fixed-header');
                        $('.header').addClass('navbar-scrolling');
                    } else {
                        $('body').removeClass('fixed-header');
                       $('.header').removeClass('navbar-scrolling');
                    }

                });
            }
        },
    };
    Core.initialize();

      /////////////////////////////////////////////////////////////////
    //   Dropdown Menu Fade
    /////////////////////////////////////////////////////////////////

    $( '#dl-menu' ).dlmenu();

    $(".yamm >li").hover(
        function() {
            $('.dropdown-menu', this).fadeIn("fast");
        },
        function() {
            $('.dropdown-menu', this).fadeOut("fast");
        });

    window.prettyPrint && prettyPrint();
    $(document).on('click', '.yamm .dropdown-menu', function(e) {
        e.stopPropagation();
    });

})();